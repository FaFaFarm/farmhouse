import axios from "axios";
import cookie from "react-cookies";

const config = require("./config.json");
let hostURL;

if (process.env.NODE_ENV == "development") {
	hostURL = config.development;
} else {
	hostURL = config.release;
}

const api = axios.create({ baseURL: hostURL });
api.interceptors.request.use((request) => requestInterceptor(request));

const requestInterceptor = (request) => {
	request.headers["x-xsrf-token"] = cookie.load(".AspNetCore.Xsrf");
	request.headers["content-type"] = "application/json";
	request.withCredentials = true;
	return request;
};
export default api;
