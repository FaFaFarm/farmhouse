import React, { Component } from "react";
import "./FarmerPage.css";
import MessageBox from "../../containers/UserInfoBoxes/MessageBox/MessageBox";
import UploadImagePopUp from "../../containers/Pop-up menus/ImageUploadPopUp/UploadImagePopUp";
import api from "../../services/api";
import defaultPhoto from "./Resources/noavatar.jpg";

class FarmerPage extends Component {
	constructor(props) {
		super(props);

		this.state = {
			displayMessageBox: false,
			isAvatarMenuShown: false,
			isAvatarUploadPopupShown: false,
			isBurgerMenuShown: false,
			aboutUserText: "",
			userName: "",
			userSurname: "",
			userAvatarImage64: "",
			userCity: "",
			userCountry: "",
			userEmail: "",
			userPhone: "",
		};

		this.handleMessageButtonClick = this.handleMessageButtonClick.bind(
			this
		);
		this.handleInfoButtonClick = this.handleInfoButtonClick.bind(this);
		this.handleProductButtonClick = this.handleProductButtonClick.bind(
			this
		);
		this.handleExitClick = this.handleExitClick.bind(this);
		this.handleMouseEnterAvatarImage = this.handleMouseEnterAvatarImage.bind(
			this
		);
		this.handleMouseLeaveAvatarImage = this.handleMouseLeaveAvatarImage.bind(
			this
		);
		this.handleClickUploadImage = this.handleClickUploadImage.bind(this);
		this.handleClosePopup = this.handleClosePopup.bind(this);
		this.handleBurgerMenuClick = this.handleBurgerMenuClick.bind(this);
	}

	componentDidMount() {
		let userId = sessionStorage.getItem("currentUserId");

		api.get("api/profile/userfullname/" + userId).then((result) => {
			if (result.data.isSuccessful) {
				this.setState({
					userName: result.data.data.json.name,
					userSurname: result.data.data.json.surname,
				});
			}
		});

		api.get("api/profile/usercountry/" + userId).then((result) => {
			if (result.data.isSuccessful) {
				this.setState({
					userCountry: result.data.data,
				});
			}
		});

		api.get("api/profile/usercity/" + userId).then((result) => {
			if (result.data.isSuccessful) {
				this.setState({
					userCity: result.data.data,
				});
			}
		});

		api.get("api/profile/userphonenumber/" + userId).then((result) => {
			if (result.data.isSuccessful) {
				this.setState({
					userPhone: result.data,
				});
			}
		});

		api.get("api/profile/useremail/" + userId).then((result) => {
			if (result.data.isSuccessful) {
				this.setState({
					userEmail: result.data.data,
				});
			}
		});

		api.get("api/profile/useravatar/" + userId).then((result) => {
			if (result.data.isSuccessful) {
				this.setState({
					userAvatarImage64: result.data.data,
				});
			}
		});
	}

	uncheckAllOptions() {
		var options = document.querySelectorAll(".menu div a");
		options.forEach((item) => {
			item.classList.remove("menu-checked");
		});
	}

	handleInfoButtonClick(e) {
		this.uncheckAllOptions();
		e.target.classList.add("menu-checked");
		this.setState({
			displayMessageBox: false,
		});
	}

	handleMessageButtonClick(e) {
		this.uncheckAllOptions();
		e.target.classList.add("menu-checked");
		this.setState(
			{
				displayMessageBox: true,
			},
			() => {
				window.scrollTo(0, document.documentElement.scrollHeight);
			}
		);
	}

	handleProductButtonClick() {}

	handleExitClick() {
		window.open("http://localhost:3000/", "_self");
	}

	handleMouseEnterAvatarImage() {
		this.setState({
			isAvatarMenuShown: true,
		});
	}

	handleMouseLeaveAvatarImage() {
		this.setState({
			isAvatarMenuShown: false,
		});
	}

	handleClickUploadImage() {
		if (!this.state.isAvatarUploadPopupShown) {
			//window.scrollTo(0, 0);
			document.body.style.overflow = "hidden";
			let parent = document.body;
			parent.classList.add("hiddent-parent");

			this.setState({
				isAvatarUploadPopupShown: true,
			});
		}
	}

	handleClosePopup(isSuccessfulUpploaded, file) {
		document.body.style.overflow = "visible";
		this.setState({
			isAvatarUploadPopupShown: false,
		});

		if (isSuccessfulUpploaded) {
			this.setState({
				userAvatarImage64: file,
			});
		}
	}

	handleBurgerMenuClick() {
		if (!this.state.isBurgerMenuShown) {
			this.setState({
				isBurgerMenuShown: true,
			});
		} else {
			this.setState({
				isBurgerMenuShown: false,
			});
		}
	}

	render() {
		let imgStyle;
		if (this.state.userAvatarImage64) {
			imgStyle = {
				"background-image": "url(" + this.state.userAvatarImage64 + ")",
			};
		} else {
			imgStyle = {
				"background-image": "url(" + defaultPhoto + ")",
			};
		}
		console.log(this.state);
		console.log(document.documentElement.clientHeight);
		console.log(document.documentElement.scrollHeight);
		return (
			<div
				className="farmer-main"
				style={{
					minHeight: window.outerHeight + "px",
				}}
			>
				<div
					className="parent"
					style={{
						minHeight: window.outerHeight + "px",
					}}
				>
					{this.state.isAvatarUploadPopupShown ? (
						<div className="darken-background">
							<UploadImagePopUp
								onClose={this.handleClosePopup}
							></UploadImagePopUp>
						</div>
					) : (
						""
					)}
					<div className="body-with-picture-container">
						<div className="menu-container">
							<div
								className={
									window.screen.width > 800 ||
									this.state.isBurgerMenuShown
										? "menu"
										: "menu-hidden"
								}
							>
								<div onClick={this.handleInfoButtonClick}>
									<a>ABOUT</a>
								</div>
								<div>
									<a>GENERAL</a>
								</div>
								<div>
									<a>COUNTER</a>
								</div>
								<div onClick={this.handleMessageButtonClick}>
									<a>MESSAGES</a>
								</div>
								<div>
									<a>TOOLS</a>
								</div>
							</div>
							{window.screen.width < 800 ? (
								<div className="burger-container">
									<input
										id="menu__toggle"
										type="checkbox"
										onClick={this.handleBurgerMenuClick}
									/>
									<label class="menu__btn" for="menu__toggle">
										<span></span>
									</label>
								</div>
							) : (
								""
							)}
						</div>
					</div>

					<div
						onMouseEnter={this.handleMouseEnterAvatarImage}
						onMouseLeave={this.handleMouseLeaveAvatarImage}
						className="avatar-image-container"
					>
						<div style={imgStyle} className="avatar-image">
							<div
								className={
									this.state.isAvatarMenuShown
										? "show-edit-image-block"
										: "hide-edit-image-block"
								}
							>
								<div
									className="edit-image-block"
									onClick={this.handleClickUploadImage}
								>
									<img
										src={require("./Resources/upload.png")}
										height="13px"
										className="edit-image-block-icon"
									/>{" "}
									Edit
								</div>
							</div>
						</div>
					</div>
					<div className="farmer-profile-container">
						<div className="information-container">
							<div className="empty-block"></div>
							<p className="name-of-the-farmer">
								{this.state.userName} {this.state.userSurname}
							</p>
							<div className="farmers-address-container">
								<p className="farmers-address">
									{this.state.userCountry},{" "}
									{this.state.userCity}
								</p>
								<p className="farmers-address">
									{this.state.userEmail}
								</p>
								<img
									src={require("./Resources/lorry.png")}
									width="40vh"
									height="40vh"
								/>
							</div>
						</div>

						<div className="achievements-container">
							<div className="progress-container">
								<div className="progress"></div>
								<div className="value-progress">100%</div>
							</div>
						</div>

						<div className="followers-and-following-container">
							<div className="followers">
								<p className="follow-count">228</p>
								<p className="follow-style">Followers</p>
							</div>
							<div className="following">
								<p className="follow-count">228</p>
								<p className="follow-style">Following</p>
							</div>
						</div>

						<div className="border">&nbsp;</div>

						{this.state.displayMessageBox ? (
							<MessageBox />
						) : (
							<div className="about-fermer-container">
								<div
									style={{
										display: "flex",
										justifyContent: "center",
									}}
								>
									<div className="occupation-container">
										<div className="occcupation">
											Fish industry,{" "}
										</div>
										<div className="occcupation">
											Flowers industry,{" "}
										</div>
										<div className="occcupation">
											Other category,{" "}
										</div>
										<div className="occcupation">
											Beekeeping...
										</div>
									</div>
								</div>
								<div className="about-fermer">
									<div className="text-about-fermer">
										Lorem Ipsum is simply dummy text of the
										printing and typesetting industry. Lorem
										Ipsum has been the industry's standard
										dummy text ever since the 1500s, when an
										unknown printer took a galley of type
										and scrambled it to make a type specimen
										book. It has survived not only five
										centuries, but also the leap into
										electronic typesetting, remaining
										essentially unchanged. It was
										popularised in the 1960s with the
										release of Letraset sheets containing
										Lorem Ipsum passages, and more recently
										with desktop publishing software like
										Aldus PageMaker including versions of
										Lorem Ipsum.
									</div>
								</div>
							</div>
						)}
					</div>
				</div>
			</div>
		);
	}
}
export default FarmerPage;
