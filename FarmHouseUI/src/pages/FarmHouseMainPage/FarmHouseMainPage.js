import React, { Component } from "react";
import Buttons from "../../components/InputFields/Buttons/Buttons";
import "./FarmHouseMainPage.css";
import Registration from "../../containers/Registration/Registration";
import Login from "../../containers/Login/Login.js";
import api from "../../services/api";

class FarmHouse extends Component {
	constructor(props) {
		super(props);

		this.state = {
			placeholder: ["Sign Up", "Sign In"],
			isFormHidden: true,
			companyText: ""
		};

		this.handleSignUpButtonClick = this.handleSignUpButtonClick.bind(this);
		this.handleSignInButtonClick = this.handleSignInButtonClick.bind(this);
		this.handleHiddenPannelToggleClick = this.handleHiddenPannelToggleClick.bind(
			this
		);
	}

	handleSignUpButtonClick() {
		document
			.getElementsByClassName("login-field")[0]
			.classList.remove("login-field-appeared");
		document
			.getElementsByClassName("login-field")[0]
			.classList.add("login-field-disappeared");
		document
			.getElementsByClassName("registration-field")[0]
			.classList.add("registration-field-appeared");
		document.getElementsByClassName("button")[1].classList.remove("active");
		document.getElementsByClassName("button")[0].classList.add("active");
	}

	handleSignInButtonClick() {
		document
			.getElementsByClassName("login-field")[0]
			.classList.add("login-field-appeared");
		document
			.getElementsByClassName("login-field")[0]
			.classList.remove("login-field-disappeared");
		document
			.getElementsByClassName("registration-field")[0]
			.classList.remove("registration-field-appeared");
		document.getElementsByClassName("button")[0].classList.remove("active");
		document.getElementsByClassName("button")[1].classList.add("active");
	}

	componentDidMount() {
		api.get("api/registrationpage/companymissiontext").then(result => {
			this.setState({
				companyText: result.data.html
			});
		});
	}

	handleHiddenPannelToggleClick() {
		let hiddenPanel;
		let toggle;
		let greeting;

		if (this.state.isFormHidden) {
			hiddenPanel = document.getElementsByClassName("hidden-panel")[0];
			toggle = document.getElementsByClassName("double-arrow")[0];
			greeting = document.getElementsByClassName("greeting")[0];
			hiddenPanel.classList.remove("hidden-panel");
			hiddenPanel.classList.add("shown-panel");
			greeting.classList.add("greeting-open");
			toggle.classList.remove("double-arrow");
			toggle.classList.add("double-arrow-rotated");

			this.setState({
				isFormHidden: false
			});
		} else {
			hiddenPanel = document.getElementsByClassName("shown-panel")[0];
			toggle = document.getElementsByClassName("double-arrow-rotated")[0];
			greeting = document.getElementsByClassName("greeting")[0];
			hiddenPanel.classList.remove("shown-panel");
			hiddenPanel.classList.add("hidden-panel");
			greeting.classList.remove("greeting-open");
			toggle.classList.remove("double-arrow-rotated");
			toggle.classList.add("double-arrow");

			this.setState({
				isFormHidden: true
			});
		}
	}

	render() {
		return (
			<div className="main">
				<div className="bg">
					<div className="hidden-panel">
						<div
							className="hidden-panel-toggle"
							onClick={this.handleHiddenPannelToggleClick}
						>
							<img
								className="double-arrow"
								src={require("./Resources/double-arrow.png")}
								alt=""
							/>
						</div>
						<div className="form-container">
							<div className="form-header">
								{" "}
								<Buttons
									placeholder={this.state.placeholder}
									onClick={[
										this.handleSignUpButtonClick,
										this.handleSignInButtonClick
									]}
								/>{" "}
							</div>
							<Registration />
							<Login />
						</div>
					</div>
					<div className="greeting">
						<h1 className="greeting-text title">FARM HOUSE</h1>
						<div className="aboutCompanyBlock">
							<p className="greeting-text aboutCompanyText">
								{this.state.companyText}
							</p>
						</div>
						<div className="border-aboutcompany-text">&nbsp;</div>
						<div className="catalog-container">
							<button className="submit-btn">Catalog</button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default FarmHouse;
