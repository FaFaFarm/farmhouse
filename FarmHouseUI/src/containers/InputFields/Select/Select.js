import React, { Component } from "react";
import SelectComponent from "../../../components/InputFields/Select/Select";

class Select extends Component {
	constructor(props) {
		super(props);

		this.state = {
			suggestions: this.props.suggestions,
			showDDL: false,
			value: this.props.placeholder
		};

		this.handleVariantClick = this.handleVariantClick.bind(this);
		this.handleSelectClick = this.handleSelectClick.bind(this);
	}

	componentDidMount() {
		document.body.addEventListener("click", e => {
			let activeBlock = ".select-input, .select-list, .select-element";
			console.log(typeof document.body.onclick + "2");
			if (!e.target.matches(activeBlock)) {
				document
					.getElementsByClassName("arrow")[0]
					.classList.remove("arrow-clicked");
				this.setState({
					showDDL: false
				});
			}
		});
	}

	handleVariantClick(e) {
		const value = e.target.innerText;
		const name = this.props.placeholder.toLowerCase();
		console.log(value);
		this.props.onChange(value, name);
		document
			.getElementsByClassName("arrow")[0]
			.classList.remove("arrow-clicked");
		this.setState({
			showDDL: false,
			value: value
		});
	}

	handleSelectClick(e) {
		if (this.state.showDDL) {
			document
				.getElementsByClassName("arrow")[0]
				.classList.remove("arrow-clicked");
			this.setState({
				showDDL: false
			});
		} else {
			document
				.getElementsByClassName("arrow")[0]
				.classList.add("arrow-clicked");
			this.setState({
				showDDL: true
			});
		}
	}

	render() {
		if (!this.state.isCloseEventSet) {
			this.setState({
				isCloseEventSet: true
			});
		}
		return (
			<SelectComponent
				isValid={this.props.isValid}
				handleSelectClick={this.handleSelectClick}
				value={this.props.value}
				suggestions={this.state.suggestions}
				showDDL={this.state.showDDL}
				handleVariantClick={this.handleVariantClick}
			/>
		);
	}
}
export default Select;
