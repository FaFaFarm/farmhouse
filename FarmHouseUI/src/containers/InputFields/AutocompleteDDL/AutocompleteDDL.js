import React, { Component } from "react";
import AutocompleteDDLComponent from "../../../components/InputFields/AutocompleteDDL/AutocompleteDDL";

class AutocompleteDDL extends Component {
	constructor(props) {
		super(props);

		this.state = {
			showDDL: false
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleClick = this.handleClick.bind(this);
	}

	componentDidMount() {
		document.body.addEventListener("click", e => {
			let activeBlock = ".drop-down-list, .DDL-element";
			console.log(typeof document.body.onclick);
			if (!e.target.matches(activeBlock)) {
				this.setState({
					showDDL: false
				});
			}
		});
	}

	handleChange(e) {
		const value = e.target.value;
		const name = this.props.placeholder.toLowerCase();
		this.props.onChange(value, name);
		this.setState({
			showDDL: true
		});
	}

	handleClick(e) {
		const value = e.currentTarget.dataset.id;
		const name = e.target.innerText;
		this.props.onClick(value, name);
		this.setState({
			showDDL: false
		});
	}

	render() {
		return (
			<AutocompleteDDLComponent
				value={this.props.value}
				isBlocked={this.props.isBlocked}
				handleChange={this.handleChange}
				placeholder={this.props.placeholder}
				showDDL={this.state.showDDL}
				suggestions={this.props.suggestions}
				handleClick={this.handleClick}
				isValid={this.props.isValid}
			/>
		);
	}
}
export default AutocompleteDDL;
