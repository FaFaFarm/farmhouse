import React, { Component } from "react";
import BurgerMenuComponent from "../../../components/InputFields/BurgerMenu/BurgerMenu";

class BurgerMenu extends Component {
	constructor(props) {
		super(props);

		this.state = {
			showMenu: false
		};

		this.handleVariantClick = this.handleVariantClick.bind(this);
		this.handleBurgerMenuClick = this.handleBurgerMenuClick.bind(this);
	}

	componentDidMount() {
		document.body.addEventListener("click", e => {
			let activeBlock =
				".BurgerMenu-list, .BurgerMenu-element, .BurgerMenu-icon, .BurgerMenu-container";
			console.log(typeof document.body.onclick + "3");
			if (!e.target.matches(activeBlock)) {
				this.setState({
					showMenu: false
				});
			}
		});
	}

	handleVariantClick(e) {
		let index = e.target.getAttribute("data-key");
		this.props.onClick[index]();
		this.setState({
			showMenu: false
		});
		window.scrollTo(0, window.screen.height);
	}

	handleBurgerMenuClick(e) {
		if (this.state.showMenu) {
			this.setState({
				showMenu: false
			});
		} else {
			this.setState({
				showMenu: true
			});
		}
	}

	render() {
		let listOfSuggestions;
		if (this.state.showMenu) {
			listOfSuggestions = (
				<ul className="BurgerMenu-list">
					{this.props.suggestions.map((suggestion, index) => {
						return (
							<li
								key={index}
								data-key={index}
								className="BurgerMenu-element"
								onClick={this.handleVariantClick}
							>
								{suggestion}
							</li>
						);
					})}
				</ul>
			);
		}
		return (
			<BurgerMenuComponent
				listOfSuggestions={listOfSuggestions}
				icon={this.props.icon}
				handleBurgerMenuClick={this.handleBurgerMenuClick}
			/>
		);
	}
}
export default BurgerMenu;
