import React, { Component } from "react";
import RegistrationComponent from "../../components/Registration/Registration";
import api from "../../services/api";

class Registration extends Component {
	constructor(props) {
		super(props);

		this.state = {
			name: "",
			surname: "",
			email: "",
			country: "",
			town: "",
			accountType: "",
			password: "",
			repPassword: "",
			countryId: 0,
			cityId: 0,
			countries: [],
			cities: [],
			isTownBlocked: true,
			isFormValid: true,
			isNameValid: true,
			isSurnameValid: true,
			isEmailValid: true,
			isCountryValid: true,
			isCityValid: true,
			isPasswordValid: true,
			isAccountTypeValid: true,
			nameInValidCause: "",
			surnameInValidCuse: "",
			emailInValidCause: "",
			passwordInValidCause: ""
		};

		this.placeholders = [
			"Name",
			"Surname",
			"Email",
			"Country",
			"Town",
			"Account Type",
			"Password(8-16)",
			"Repeat"
		];
		this.accTypes = ["Farmer", "Customer"];

		this.handleTextChange = this.handleTextChange.bind(this);
		this.handleCountryTextChange = this.handleCountryTextChange.bind(this);
		this.checkPassword = this.checkPassword.bind(this);
		this.handleCountryClick = this.handleCountryClick.bind(this);
		this.handleCityTextChange = this.handleCityTextChange.bind(this);
		this.handleCityClick = this.handleCityClick.bind(this);
		this.handleSignUpClick = this.handleSignUpClick.bind(this);
		this.handleAccountTypeChange = this.handleAccountTypeChange.bind(this);
		this.handlePasswordChange = this.handlePasswordChange.bind(this);
	}

	handleTextChange(value, name) {
		this.setState({
			[name]: value
		});
		//console.log(this.state[name]);
	}

	handleCountryTextChange(value, name) {
		this.setState({
			[name]: value
		});
		let gotCountries = [];
		if (value.length > 0) {
			api.get("api/country/CountriesAutocomplite/" + value).then(
				result => {
					if (result.data[0] !== undefined) {
						result.data.forEach(element => {
							gotCountries.push({
								lable: element.name,
								id: element.id
							});
						});
						this.setState({
							countries: gotCountries
						});
					}
				}
			);
		}

		if (value.length === 0) {
			this.setState({
				isTownBlocked: true
			});
		}
	}

	handleCountryClick(value, name) {
		this.setState({
			country: name,
			countryId: value
		});

		if (this.state.country.length > 0 && value.length > 0) {
			this.setState({
				isTownBlocked: false
			});
		}
	}

	handleAccountTypeChange(value, name) {
		this.setState({
			accountType: value
		});
	}

	handleCityTextChange(value, name) {
		this.setState({
			[name]: value
		});

		let gotCities = [];
		if (value.length > 0) {
			api.get(
				"api/city/citiesbycountryautocomplite/" +
					value +
					"/" +
					this.state.countryId
			).then(result => {
				if (result.data[0] !== undefined) {
					result.data.forEach(element => {
						gotCities.push({ lable: element.name, id: element.id });
					});
					this.setState({
						cities: gotCities
					});
				}
			});
		}
	}

	handleCityClick(value, name) {
		this.setState({
			town: name,
			cityId: value
		});
	}

	checkPassword(value, name) {
		if (
			value !== this.state.password ||
			value.length < 8 ||
			value.length > 16
		) {
			this.setState({
				isPasswordValid: false
			});
		} else {
			this.setState({
				isPasswordValid: true,
				password: value,
				repPassword: value
			});
		}
	}

	handlePasswordChange(value, name) {
		this.setState({
			password: value
		});
	}

	handleSignUpClick() {
		let namePattern = /^\p{L}{1,20}-?\p{L}{1,20}$/u;
		let emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		let isNameValid = namePattern.test(
			String(this.state.name).toLowerCase()
		);
		let isSurnameValid = namePattern.test(
			String(this.state.surname).toLowerCase()
		);
		let isEmailValid = emailPattern.test(
			String(this.state.email).toLowerCase()
		);
		let isCountryValid = this.state.country.length > 0;
		let isCityValid = this.state.town.length > 0 && this.state.cityId !== 0;
		let isAccountTypeValid =
			this.state.accountType.toLocaleLowerCase() === "farmer" ||
			this.state.accountType.toLocaleLowerCase() === "customer";
		let isPasswordValid =
			this.state.password.length > 0 && this.state.isPasswordValid;

		let isFormValid =
			isNameValid &&
			isSurnameValid &&
			isEmailValid &&
			isCountryValid &&
			isCityValid &&
			this.state.isPasswordValid &&
			isAccountTypeValid;

		this.setState({
			isCityValid: isCityValid,
			isAccountTypeValid: isAccountTypeValid,
			isCountryValid: isCountryValid,
			isEmailValid: isEmailValid,
			isNameValid: isNameValid,
			isSurnameValid: isSurnameValid,
			isFormValid: isFormValid,
			isPasswordValid: isPasswordValid
		});

		if (!isFormValid) {
			this.setValidation(
				"name",
				isNameValid,
				"Name cannot be empty",
				"Enter your real name",
				"",
				"nameInValidCause",
				this.state.name.length
			);

			this.setValidation(
				"surname",
				isSurnameValid,
				"Surname cannot be empty",
				"Enter your real surname",
				"",
				"surnameInValidCuse",
				this.state.surname.length
			);

			this.setValidation(
				"email",
				isEmailValid,
				"Email cannot be empty",
				"Enter your real email",
				"This email is already taken",
				"emailInValidCause",
				this.state.email.length,
				false
			);

			this.setValidation(
				"password",
				isPasswordValid,
				"",
				"",
				"",
				"",
				this.state.password.length
			);
		} else {
			document
				.getElementsByClassName("preloader-reg")[0]
				.classList.add("appeared");
			let data = {
				Name: this.state.name,
				Surname: this.state.surname,
				Password: this.state.password,
				Email: this.state.email,
				CityId: parseInt(this.state.cityId),
				CountryId: parseInt(this.state.countryId),
				AccountType: this.state.accountType
			};
			api.post("api/registration/register/", data).then(
				result => {
					console.log(result);
					document
						.getElementsByClassName("preloader-reg")[0]
						.classList.remove("appeared");
					if (result.data.isSuccessful) {
						document
							.getElementsByClassName("affirmative-message")[0]
							.classList.add("appeared");
					} else if (result.data.statusCode === 404) {
						this.setValidation(
							"email",
							false,
							"Email cannot be empty",
							"Enter your real email",
							"This email is already taken",
							"emailInValidCause",
							this.state.email.length,
							true
						);
					}
				},
				error => {
					console.log(error);
				}
			);
		}
	}

	setValidation(
		name,
		isValid,
		emptyMess,
		secondMess,
		thirdMess,
		cause,
		emptyVal,
		isTaken
	) {
		if (!isValid) {
			this.setState({
				[name]: ""
			});

			if (emptyVal === 0) {
				this.setState({
					[cause]: emptyMess
				});
			} else if (isTaken) {
				this.setState({
					isEmailValid: false,
					[cause]: thirdMess
				});
			} else {
				this.setState({
					[cause]: secondMess
				});
			}
		}
	}

	render() {
		return (
			<RegistrationComponent
				state={this.state}
				placeholders={this.placeholders}
				handleTextChange={this.handleTextChange}
				handleCountryClick={this.handleCountryClick}
				handleCountryTextChange={this.handleCountryTextChange}
				handleCityClick={this.handleCityClick}
				handleCityTextChange={this.handleCityTextChange}
				handleAccountTypeChange={this.handleAccountTypeChange}
				handleSignUpClick={this.handleSignUpClick}
				checkPassword={this.checkPassword}
				handlePasswordChange={this.handlePasswordChange}
				accTypes={this.accTypes}
			/>
		);
	}
}
export default Registration;
