import React, { Component } from "react";
import ImageUpload from "../../../components/Pop-up menus/ImageUploadPopUp/ImageUpload";
import "./UploadImagePopUp.css";
import api from "../../../services/api";

class UploadImagePopUp extends Component {
	constructor(props) {
		super(props);

		this.state = {
			files: [],
			croppedFile: "",
			isSuccessfulUploaded: false
		};

		this.handleUploadingNewAvatarImageClick = this.handleUploadingNewAvatarImageClick.bind(
			this
		);
		this.handleCropComplete = this.handleCropComplete.bind(this);
	}

	handleUploadingNewAvatarImageClick() {
		let form = new FormData();
		form.append("file", this.state.croppedFile);

		let data = {
			Document: this.state.croppedFile,
			UserId: parseInt(sessionStorage.getItem("currentUserId")),
			Description: ""
		};

		api.post("api/profile/uploadavatarimage/", data).then(
			result => {

				if (result.data.isSuccessful) {
					this.props.onClose(true, this.state.croppedFile)
				}
			},
			error => {
				console.log(error);
			}
		);

		this.props.onClose(false);
		
	}

	async handleCropComplete(crop) {
		let finalImage = await this.getCroppedImg(
			document.getElementsByClassName("ReactCrop__image")[0],
			crop,
			"NewImage.jpeg"
		);

		if (finalImage != null) {
			this.setState({
				croppedFile: finalImage
			});
		}
	}

	getCroppedImg(image, pixelCrop, fileName) {
		const canvas = document.createElement("canvas");
		canvas.width = pixelCrop.width;
		canvas.height = pixelCrop.height;
		const ctx = canvas.getContext("2d");
		var _image = new Image();
		_image.src = image.src;
		let widthProportions = image.naturalWidth / image.width;
		let heightProportions = image.naturalHeight / image.height;
		ctx.drawImage(
			_image,
			pixelCrop.x * widthProportions,
			pixelCrop.y * heightProportions,
			pixelCrop.width * Math.pow(widthProportions, 2),
			pixelCrop.height * Math.pow(heightProportions, 2),
			0,
			0,
			pixelCrop.width * widthProportions,
			pixelCrop.height * heightProportions
		);

		if (canvas != null) {
			return canvas.toDataURL("image/jpeg");
		}
	}

	render() {
		return (
			<ImageUpload
				onUploadImageClick={this.handleUploadingNewAvatarImageClick}
				onCropComplete={this.handleCropComplete}
				files={this.state.files}
				onClose={this.props.onClose}
			/>
		);
	}
}

export default UploadImagePopUp;
