import React, { Component } from "react";
import LoginComponent from "../../components/Login/Login";
import api from "../../services/api";

class Login extends Component {
	constructor(props) {
		super(props);

		this.state = {
			email: "",
			password: "",
			isEmailValid: true,
			isPasswordValid: true,
			emailInValidCause: "",
			passwordInValidCause: ""
		};
		this.placeholders = ["Email", "Password"];

		this.handleTextChange = this.handleTextChange.bind(this);
		this.handleSignInClick = this.handleSignInClick.bind(this);
		this.setValidation = this.setValidation.bind(this);
	}

	handleTextChange(value, name) {
		console.log(this.state.password);
		this.setState({
			[name]: value
		});
	}

	handleSignInClick(e) {
		e.preventDefault();
		let emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		let isEmailValid = emailPattern.test(
			String(this.state.email).toLowerCase()
		);
		let isPasswordValid = this.state.password.length > 0;
		let isFormValid = isEmailValid && isPasswordValid;
		this.setState({
			isEmailValid: isEmailValid,
			isPasswordValid: isPasswordValid
		});
		if (!isFormValid) {
			this.setValidation(
				"email",
				isEmailValid,
				"Email cannot be empty",
				"Enter your real email",
				"This email is already taken",
				"emailInValidCause",
				this.state.email.length,
				false
			);

			this.setValidation(
				"password",
				isPasswordValid,
				"Password cannot be empty",
				"",
				"",
				"passwordInValidCause",
				this.state.password.length,
				false
			);
		} else {
			document
				.getElementsByClassName("preloader-log")[0]
				.classList.add("appeared");
			let data = {
				Email: this.state.email,
				Password: this.state.password
			};
			api.post("api/authentication/authenticate/", data)
				.then(function(result) {
					console.log(result);
					document
						.getElementsByClassName("preloader-log")[0]
						.classList.remove("appeared");
					if (result.data.isSuccessful) {
						console.log(document.cookie);
						sessionStorage.setItem(
							"currentUserId",
							result.data.data.id
						);
						window.open(
							"http://localhost:3000/farmer/" +
								result.data.data.id,
							"_self"
						);
					} else {
						switch (result.data.errors[0]) {
							case "Incorrect email entered": {
								this.setValidation(
									"email",
									false,
									"Email cannot be empty",
									"Enter your real email",
									"Incorrect email entered",
									"emailInValidCause",
									this.state.email.length,
									true,
									"isEmailValid"
								);
								break;
							}

							case "Incorrect password entered": {
								this.setValidation(
									"password",
									false,
									"Password cannot be empty",
									"",
									"Incorrect password entered",
									"passwordInValidCause",
									this.state.password.length,
									true,
									"isPasswordValid"
								);
								break;
							}
							default:
						}
					}
				})
				.catch(function(error) {
					console.log(error);
				});
		}
	}

	setValidation(
		name,
		isValid,
		emptyMess,
		secondMess,
		thirdMess,
		cause,
		emptyVal,
		isTaken,
		toValidate
	) {
		if (!isValid) {
			this.setState({
				[name]: ""
			});

			if (emptyVal === 0) {
				this.setState({
					[cause]: emptyMess
				});
			} else if (isTaken) {
				this.setState({
					[toValidate]: false,
					[cause]: thirdMess
				});
			} else {
				this.setState({
					[cause]: secondMess
				});
			}
		}
	}

	render() {
		return (
			<LoginComponent
				state={this.state}
				placeholders={this.placeholders}
				handleSignInClick={this.handleSignInClick}
				handleTextChange={this.handleTextChange}
			/>
		);
	}
}
export default Login;
