import React, { Component } from "react";
import MessageBoxComponent from "../../../components/UserInfoBoxes/MessageBox/MessageBox";
import api from "../../../services/api";

class MessageBox extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isDialogBoxOpened: false,
			userSearchQuery: "",
			suggestedUsers: [],
			targetUserId: "",
			targetUserName: "",
			targetUserSurname: "",
			currentUserId: "",
			existingDialogs: [
				{
					id: 2,
					name: "Vladik",
					surname: "Raginya",
					message: "Я Владик, а ты хто?"
				},
				{
					id: 3,
					name: "Lev",
					surname: "Kostyuchenko",
					message: "Огурец"
				}
			]
		};

		this.handleMessageClick = this.handleMessageClick.bind(this);
		this.handleExitClick = this.handleExitClick.bind(this);
		this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
	}

	handleSearchTextChange(value) {
		this.setState({
			userSearchQuery: value
		});

		let gotUsers = [];
		api.get(
			"api/Search/SearchUserBySubString/" +
				sessionStorage.getItem("currentUserId") +
				"/" +
				value
		)
			.then(result => {
				if (result.data.isSuccessful) {
					result.data.data.forEach(element => {
						gotUsers.push({
							name: element.name,
							surname: element.surname,
							id: element.userId
						});
					});
				}
				this.setState({
					suggestedUsers: gotUsers
				});
			})
			.catch(error => {
				this.setState({
					suggestedUsers: gotUsers
				});
			});
	}

	handleMessageClick(suggestion) {
		// //     let dark = document.createElement("div");
		// //     console.log(suggestion);
		// //     dark.innerHTML = "&nbsp;";
		// //    //dark.classList.add("darken-background");
		//    document.body.appendChild(dark);
		this.setState({
			isDialogBoxOpened: true,
			targetUserId: suggestion.id,
			targetUserName: suggestion.name,
			targetUserSurname: suggestion.surname
		});
	}

	handleExitClick(e) {
		//document.getElementsByClassName("darken-background")[0].remove();
		this.setState({
			isDialogBoxOpened: false
		});
	}

	render() {
		return (
			<MessageBoxComponent
				state={this.state}
				handleExitClick={this.handleExitClick}
				handleMessageClick={this.handleMessageClick}
				handleSearchTextChange={this.handleSearchTextChange}
			/>
		);
	}
}
export default MessageBox;
