import React, { Component } from "react";
import * as signalR from "@microsoft/signalr";
import DialogBoxComponent from "../../../components/UserInfoBoxes/DialogBox/DialogBox";
import api from "../../../services/api";
import Axios from "axios";

class DialogBox extends Component {
	constructor(props) {
		super(props);

		this.state = {
			userMessage: "",
			hubConnection: "",
			receivedMessages: [],
			sentMessages: [],
			isOpponentInfoShown: false,
			opponentInfo: {}
		};

		this.handleTextChange = this.handleTextChange.bind(this);
		this.handleSendMessageEvent = this.handleSendMessageEvent.bind(this);
		this.resizeWorkspace = this.resizeWorkspace.bind(this);
		this.scrollDownMessageContainer = this.scrollDownMessageContainer.bind(
			this
		);
		this.handleAvatarClick = this.handleAvatarClick.bind(this);
	}

	componentDidMount() {
		let messageField = document.getElementsByClassName(
			"dialog-messages-container"
		)[0];
		console.log(messageField);
		messageField.addEventListener("click", e => {
			let activeBlock =
				".dialog-message-block, .dialog-message-text, .message-icon";
			console.log(typeof document.body.onclick);
			if (!e.target.matches(activeBlock)) {
				this.setState({
					isOpponentInfoShown: false
				});
			}
		});
		let textarea = document.getElementsByTagName("textarea")[0];
		textarea.addEventListener("keydown", e => {
			if (e.keyCode === 13) {
				e.preventDefault();
				if (this.state.userMessage !== "") {
					this.handleSendMessageEvent(e);
				}
			}
		});

		let connection = new signalR.HubConnectionBuilder()
			.withUrl("https://localhost:44340/chat")
			.configureLogging(signalR.LogLevel.Information)
			.build();

		connection.start();

		this.setState({
			hubConnection: connection
		});

		connection.on("ReceiveTextMessage", (data, isCurrentUserMessage) => {
			let allMessages = this.state.receivedMessages;
			allMessages.push({
				message: data,
				isReceivedByCurrentUser: isCurrentUserMessage
			});
			this.setState({
				receivedMessages: allMessages
			});
			this.scrollDownMessageContainer();
		});

		connection.on("GetAllMessages", messagesList => {
			this.setState({
				receivedMessages: messagesList.data[0]
			});
			this.scrollDownMessageContainer();
		});
	}

	scrollDownMessageContainer() {
		let messagesContainer = document.getElementsByClassName(
			"dialog-messages-container"
		)[0];
		messagesContainer.scrollTop = messagesContainer.scrollHeight;
		console.log(messagesContainer.scrollTop);
	}

	handleTextChange(e) {
		this.resizeWorkspace();
		this.setState({
			userMessage: e.target.value
		});
	}

	handleAvatarClick(e) {
		let opponentData = {};
		Axios.all([
			api.get("api/Profile/UserCountry/" + this.props.targetUserId),
			api.get("api/Profile/UserCity/" + this.props.targetUserId),
			api.get("api/Profile/UserPhoneNumber/" + this.props.targetUserId),
			api.get("api/Profile/UserDateOfBirth/" + this.props.targetUserId),
			api.get("api/Profile/UserEmail/" + this.props.targetUserId)
		]).then(
			Axios.spread((...results) => {
				console.log(results);
				opponentData["country"] = results[0].data.data;
				opponentData["city"] = results[1].data.data;
				opponentData["phone"] = results[2].data.data;
				opponentData["dateOfBirth"] = results[3].data.data.split(
					" "
				)[0];
				opponentData["email"] = results[4].data.data;
				this.setState({
					isOpponentInfoShown: true,
					opponentInfo: opponentData
				});
			})
		);
	}

	resizeWorkspace() {
		let workspace = document.getElementsByName("workspace")[0];
		let textarea = document.getElementsByTagName("textarea")[0];
		textarea.style.height = 0 + "px";
		if (textarea.scrollHeight < 100) {
			textarea.style.height = textarea.scrollHeight + "px";
			workspace.style.height =
				parseInt(textarea.style.height) + 31 + "px";
		} else {
			textarea.style.height =
				parseInt(workspace.style.height) - 31 + "px";
			textarea.style.overflowY = "auto";
		}
		console.log(textarea.scrollHeight);
	}

	handleSendMessageEvent(e) {
		let textMessage = {
			ToUserId: this.props.targetUserId,
			Message: this.state.userMessage
		};
		this.state.hubConnection.invoke("SendTextMessageToUser", textMessage);
		this.state.sentMessages.push(textMessage);
		this.setState({
			userMessage: ""
		});
		this.resizeWorkspace();
	}

	render() {
		return (
			<DialogBoxComponent
				state={this.state}
				handleAvatarClick={this.handleAvatarClick}
				handleTextChange={this.handleTextChange}
				handleSendMessageEvent={this.handleSendMessageEvent}
				targetUserName={this.props.targetUserName}
				targetUserSurname={this.props.targetUserSurname}
			/>
		);
	}
}
export default DialogBox;
