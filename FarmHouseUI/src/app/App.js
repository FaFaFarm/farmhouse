import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import FarmHouse from "../pages/FarmHouseMainPage/FarmHouseMainPage";
import FarmerPage from "../pages/FarmerPage/FarmerPage.js";

function App() {
	return (
		<Router>
			<Switch>
				<Route exact path="/">
					<FarmHouse />
				</Route>
				<Route path="/farmer/:id">
					<FarmerPage />
				</Route>
			</Switch>
		</Router>
	);
}

export default App;
