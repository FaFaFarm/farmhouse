import React from "react";
import "./Login.css";
import TextInput from "../InputFields/TextInput/textInput";
import PasswordInput from "../InputFields/PasswordInput/passwordInput";

function LoginComponent(props) {
	let { state, placeholders, handleSignInClick, handleTextChange } = props;

	return (
		<div className="login-field">
			<img
				src={require("./Resources/user-icon.png")}
				alt="Login"
				className="login-icon"
			/>
			<img
				//src={require("./Resources/preloader.gif")}
				alt=""
				className="preloader-log"
			/>
			<form className="login-field-input">
				<div
					className={
						state.isEmailValid ? "input-wrapper" : "invalid-field"
					}
				>
					<img
						src={require("./Resources/email.png")}
						alt="Email"
						className="email-icon"
					/>
					<TextInput
						type="text"
						value={state.email}
						placeholder={placeholders[0]}
						onChange={handleTextChange}
						isValid={state.isEmailValid}
						inValidCause={state.emailInValidCause}
					/>
				</div>
				<div
					className={
						state.isPasswordValid
							? "input-wrapper"
							: "invalid-field"
					}
				>
					<img
						src={require("./Resources/lock.png")}
						alt="Password"
						className="password-icon"
					/>
					<PasswordInput
						type="password"
						value={state.password}
						onChange={handleTextChange}
						placeholder={placeholders[1]}
						isValid={state.isPasswordValid}
						inValidCause={state.passwordInValidCause}
					/>
				</div>
				<button className="submit-btn" onClick={handleSignInClick}>
					Sign In
				</button>
			</form>
		</div>
	);
}
export default LoginComponent;
