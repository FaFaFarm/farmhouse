import React from "react";
import "./Select.css";

function SelectComponent(props) {
	let {
		isValid,
		handleSelectClick,
		value,
		suggestions,
		showDDL,
		handleVariantClick
	} = props;

	let listOfSuggestions;
	if (showDDL) {
		listOfSuggestions = (
			<ul className="select-list">
				{suggestions.map((suggestion, index) => {
					return (
						<li
							className="select-element"
							key={suggestion}
							onClick={handleVariantClick}
						>
							{suggestion}
						</li>
					);
				})}
			</ul>
		);
	}
	if (!isValid) {
		return (
			<div className="select-container">
				<div className="arrow"></div>
				<div
					className="select-input-invalid"
					onClick={handleSelectClick}
				>
					{value}
				</div>
				{listOfSuggestions}
			</div>
		);
	} else {
		return (
			<div className="select-container">
				<div className="arrow"></div>
				<div className="select-input" onClick={handleSelectClick}>
					{value}
				</div>
				{listOfSuggestions}
			</div>
		);
	}
}
export default SelectComponent;
