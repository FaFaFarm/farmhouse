import React from "react";
import "./BurgerMenu.css";

function BurgerMenuComponent(props) {
	let { listOfSuggestions, icon, handleBurgerMenuClick } = props;

	return (
		<div className="BurgerMenu-container">
			<img
				src={icon}
				alt="burger"
				className="BurgerMenu-icon"
				onClick={handleBurgerMenuClick}
			/>
			{listOfSuggestions}
		</div>
	);
}
export default BurgerMenuComponent;
