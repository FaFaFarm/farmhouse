import React from "react";
import "./passwordInput.css";

export default function PasswordInput(props) {
	function handleChange(e) {
		const value = e.target.value;
		const name = props.placeholder.toLowerCase();
		props.onChange(value, name);
	}

	console.log(props.inValidCause);

	if (!props.isValid) {
		return (
			<input
				className="invalid-password-input"
				type={props.type}
				placeholder={props.inValidCause}
				onChange={handleChange}
			/>
		);
	} else {
		return (
			<input
				type={props.type}
				placeholder={props.placeholder}
				onChange={handleChange}
			/>
		);
	}
}
