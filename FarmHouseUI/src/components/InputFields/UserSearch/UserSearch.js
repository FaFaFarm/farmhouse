import React from "react";
import "./UserSearch.css";

function UserSearch(props) {
	function handleChange(e) {
		const value = e.target.value;
		props.onChange(value);
	}

	function handleFocus(e) {
		document
			.getElementsByClassName("search-wrapper")[0]
			.classList.add("focus");
	}

	function handleBlur(e) {
		document
			.getElementsByClassName("search-wrapper")[0]
			.classList.remove("focus");
	}

	return (
		<div className="search-container">
			<div className="search-wrapper">
				<img
					src={require("./Resources/search.png")}
					alt=""
					className="search-icon"
				></img>
				<input
					value={props.value}
					className="search-user"
					onChange={handleChange}
					placeholder="Search"
					onFocus={handleFocus}
					onBlur={handleBlur}
				/>
			</div>
		</div>
	);
}
export default UserSearch;
