import React from "react";
import "./Buttons.css";

export default function ButtonsComponent(props) {
	return (
		<ul>
			<li>
				<a className="button" onClick={props.onClick[0]}>
					{props.placeholder[0]}
				</a>
			</li>
			<li>
				<a className="button active" onClick={props.onClick[1]}>
					{props.placeholder[1]}
				</a>
			</li>
		</ul>
	);
}
