import React from "react";
import "./textInput.css";

export default function TextInput(props) {
	function handleChange(e) {
		const value = e.target.value;
		const name = props.placeholder.toLowerCase();
		props.onChange(value, name);
	}

	if (props.isValid) {
		return (
			<input
				value={props.value}
				type={props.type}
				placeholder={props.placeholder}
				onChange={handleChange}
			/>
		);
	} else {
		return (
			<input
				value={props.value}
				type={props.type}
				placeholder={props.inValidCause}
				onChange={handleChange}
				className="invalidfield"
			/>
		);
	}
}
