import React from "react";
import "./AutocompleteDDL.css";

function AutocompleteDDLComponent(props) {
	let {
		value,
		isBlocked,
		handleChange,
		placeholder,
		showDDL,
		suggestions,
		handleClick,
		isValid
	} = props;

	let listOfSuggestions;
	if (value !== "" && showDDL && !isBlocked) {
		listOfSuggestions = (
			<ul className="drop-down-list">
				{suggestions.map(suggestion => {
					return (
						<li
							className="DDL-element"
							key={suggestion.id}
							data-id={suggestion.id}
							onClick={handleClick}
						>
							{suggestion.lable}
						</li>
					);
				})}
			</ul>
		);
	}

	if (isBlocked) {
		return (
			<div className="DDL-container">
				<input
					className="DDL-input-blocked"
					type="text"
					value={placeholder}
					readOnly="readnly"
				/>
			</div>
		);
	} else if (!isValid) {
		return (
			<div className="DDL-container">
				<input
					className="DDL-input-inValid"
					type="text"
					value={value}
					placeholder={placeholder}
					onChange={handleChange}
				/>
				{listOfSuggestions}
			</div>
		);
	} else {
		return (
			<div className="DDL-container">
				<input
					className="DDL-input"
					type="text"
					value={value}
					placeholder={placeholder}
					onChange={handleChange}
				/>
				{listOfSuggestions}
			</div>
		);
	}
}

export default AutocompleteDDLComponent;
