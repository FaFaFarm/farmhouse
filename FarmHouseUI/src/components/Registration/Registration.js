import React from "react";
import "./Registration.css";
import TextInput from "../InputFields/TextInput/textInput";
import AutocompleteDDL from "../../containers/InputFields/AutocompleteDDL/AutocompleteDDL";
import PasswordInput from "../InputFields/PasswordInput/passwordInput";
import Select from "../../containers/InputFields/Select/Select";

function RegistrationComponent(props) {
	let {
		state,
		placeholders,
		handleTextChange,
		handleCountryClick,
		handleCountryTextChange,
		handleCityClick,
		handleCityTextChange,
		handleAccountTypeChange,
		handleSignUpClick,
		checkPassword,
		handlePasswordChange,
		accTypes
	} = props;

	return (
		<div className="registration-field">
			<div id="1" className="affirmative-message">
				Registration Successful
			</div>
			<img
				//src={require("./../preloader.gif")}
				alt=""
				className="preloader-reg"
			/>
			<form className="registration-field-input">
				<TextInput
					type="text"
					value={state.name}
					placeholder={placeholders[0]}
					onChange={handleTextChange}
					isValid={state.isNameValid}
					inValidCause={state.nameInValidCause}
				/>
				<TextInput
					type="text"
					value={state.surname}
					placeholder={placeholders[1]}
					onChange={handleTextChange}
					isValid={state.isSurnameValid}
					inValidCause={state.surnameInValidCuse}
				/>
				<TextInput
					type="text"
					value={state.email}
					placeholder={placeholders[2]}
					onChange={handleTextChange}
					isValid={state.isEmailValid}
					inValidCause={state.emailInValidCause}
				/>
				<AutocompleteDDL
					value={state.country}
					placeholder={placeholders[3]}
					onClick={handleCountryClick}
					onChange={handleCountryTextChange}
					suggestions={state.countries}
					isValid={state.isCountryValid}
				/>
				<AutocompleteDDL
					value={state.town}
					placeholder={placeholders[4]}
					onClick={handleCityClick}
					onChange={handleCityTextChange}
					isBlocked={state.isTownBlocked}
					suggestions={state.cities}
					isValid={state.isCityValid}
				/>
				<Select
					value={state.accountType}
					placeholder={placeholders[5]}
					onChange={handleAccountTypeChange}
					suggestions={accTypes}
					isValid={state.isAccountTypeValid}
				/>
				<div className="password-fields">
					<PasswordInput
						type="password"
						onChange={handlePasswordChange}
						placeholder={placeholders[6]}
						isValid={state.isPasswordValid}
						inValidCause={state.passwordInValidCause}
					/>
					<PasswordInput
						type="password"
						onChange={checkPassword}
						placeholder={placeholders[7]}
						isValid={state.isPasswordValid}
						inValidCause={state.passwordInValidCause}
					/>
				</div>
				<button
					type="button"
					className="submit-btn"
					onClick={handleSignUpClick}
				>
					Sign Up
				</button>
			</form>
		</div>
	);
}
export default RegistrationComponent;
