import React from "react";
import "./MessageBox.css";
import DialogBox from "../../../containers/UserInfoBoxes/DialogBox/DialogBox";
import UserSearch from "../../InputFields/UserSearch/UserSearch";

function MessageBoxComponent(props) {
	let {
		state,
		handleExitClick,
		handleMessageClick,
		handleSearchTextChange
	} = props;

	let messageList;
	console.log(state.existingDialogs);
	messageList = (
		<ul className="search-user-list">
			{state.userSearchQuery === ""
				? state.existingDialogs.map(suggestion => {
						return (
							<li
								className="message-block"
								key={suggestion.id}
								data-id={suggestion.id}
								onClick={() => handleMessageClick(suggestion)}
							>
								<img
									src={require("./Resources/)).jpg")}
									alt=""
									className="message-icon"
								></img>
								<div className="message-main">
									<div className="message-author">
										{suggestion.name +
											" " +
											suggestion.surname}
									</div>
									<div className="message-text">
										{suggestion.message}
									</div>
								</div>
							</li>
						);
				  })
				: state.existingDialogs
						.filter(element => {
							let fullName =
								element.name.toLowerCase() +
								" " +
								element.surname.toLowerCase();
							return fullName.startsWith(
								state.userSearchQuery.toLowerCase()
							);
						})
						.map(suggestion => {
							return (
								<li
									className="message-block"
									key={suggestion.id}
									data-id={suggestion.id}
									onClick={() =>
										handleMessageClick(suggestion)
									}
								>
									<img
										src={require("./Resources/)).jpg")}
										alt=""
										className="message-icon"
									></img>
									<div className="message-main">
										<div className="message-author">
											{suggestion.name +
												" " +
												suggestion.surname}
										</div>
										<div className="message-text">
											{suggestion.message}
										</div>
									</div>
								</li>
							);
						})}
		</ul>
	);
	let userList;
	if (state.userSearchQuery !== "") {
		userList = (
			<ul className="search-user-list">
				<div className="user-list-label">Suggested Users</div>
				{state.suggestedUsers.map(suggestion => {
					return (
						<li
							className="message-block"
							key={suggestion.id}
							data-id={suggestion.id}
							onClick={() => handleMessageClick(suggestion)}
						>
							<img
								src={require("./Resources/))).jpg")}
								alt=""
								className="message-icon"
							></img>
							<div className="message-main">
								{suggestion.name + " " + suggestion.surname}
							</div>
						</li>
					);
				})}
			</ul>
		);
	}
	let dialog;
	if (state.isDialogBoxOpened) {
		dialog = (
			<DialogBox
				exit={handleExitClick}
				targetUserId={state.targetUserId}
				targetUserName={state.targetUserName}
				targetUserSurname={state.targetUserSurname}
			/>
		);
	}
	return (
		<div className="box-container">
			<div className="message-box">
				<UserSearch
					value={state.userSearchQuery}
					onClick={handleMessageClick}
					onChange={handleSearchTextChange}
				/>
				<div className="box-content">
					<div className="user-list-label">Your dialogs</div>
					{messageList}
					{userList}
				</div>
			</div>
			{dialog}
		</div>
	);
}
export default MessageBoxComponent;
