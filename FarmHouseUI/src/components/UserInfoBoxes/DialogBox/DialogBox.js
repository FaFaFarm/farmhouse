import React from "react";
import "./DialogBox.css";

function DialogBoxComponent(props) {
	let {
		state,
		handleAvatarClick,
		handleTextChange,
		handleSendMessageEvent,
		targetUserName,
		targetUserSurname
	} = props;

	console.log(state.isOpponentInfoShown);
	let messageList;
	messageList = state.receivedMessages.map(suggestion => {
		//console.log(suggestion)
		if (suggestion.isReceivedByCurrentUser) {
			return (
				<div className="dialog-message-block self">
					<div className="dialog-message-text">
						{suggestion.message}
					</div>
				</div>
			);
		} else {
			return (
				<div className="dialog-message-block opponent">
					<img
						src={require("./Resources/)).jpg")}
						alt=""
						className="message-icon"
						onClick={handleAvatarClick}
					></img>
					<div className="dialog-message-text">
						{suggestion.message}
					</div>
				</div>
			);
		}
	});
	let opponentInfo = (
		<div className="opponent-info-container">
			<img
				src={require("./Resources/)).jpg")}
				alt=""
				className="opponent-icon"
			></img>
			<div className="opponent-base-info">
				<div className="opponent-name">
					{props.targetUserName + " " + props.targetUserSurname}
				</div>
				<div className="opponent-country">
					{state.opponentInfo.country + "/" + state.opponentInfo.city}
				</div>
			</div>
			<div className="opponent-phone">
				{"Tel: " + state.opponentInfo.phone}
			</div>
			<div className="opponent-date-of-birth">
				{"DOB: " + state.opponentInfo.dateOfBirth}
			</div>
			<div className="opponent-email">
				{"Email: " + state.opponentInfo.email}
			</div>
		</div>
	);
	return (
		<form className="dialog-form" method="post" action="send/to/server">
			<div className="dialog-box">
				<div className="dialog-header">
					<div className="dialog-opponent">
						{targetUserName + " " + targetUserSurname}
					</div>
					{/*<img src={require('./exit.png')} alt="" className="exit-icon" onClick = {props.exit}></img>*/}
				</div>
				<div className="dialog-messages-container">{messageList}</div>
				<div
					className="dialog-workspace"
					name="workspace"
					style={{ height: "50px" }}
				>
					<img
						src={require("./Resources/paperclip.png")}
						alt=""
						className="attach-icon"
					></img>
					<textarea
						value={state.userMessage}
						cols="35"
						rows="2"
						placeholder="Write here"
						onChange={handleTextChange}
					/>
					<img
						src={require("./Resources/smile.png")}
						alt=""
						className="smile-icon"
					></img>
					<img
						src={require("./Resources/send.png")}
						alt=""
						className="send-icon"
						onClick={handleSendMessageEvent}
					></img>
				</div>
			</div>
			{state.isOpponentInfoShown ? opponentInfo : ""}
		</form>
	);
}

export default DialogBoxComponent;
