import React, { Component } from "react";
import Dropzone from "react-dropzone";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import "./ImageUpload.css";

class ImageUpload extends Component {
	constructor(props) {
		super(props);

		this.state = {
			files: [],
			src: null,
			crop: {
				aspect: 1 / 1,
				x: 10,
				y: 10,
				width: 80,
				height: 80
			}
		};

		this.onDrop = this.onDrop.bind(this);
		this.handleCancelUploadingNewAvatarImageClick = this.handleCancelUploadingNewAvatarImageClick.bind(
			this
		);
	}

	componentDidMount() {
		document
			.getElementsByClassName("darken-background")[0]
			.addEventListener("click", e => {
				let activeBlock =
					".popup-container, .popup-container-header, .popup-body, input, #invite-text, p, b, button, .crop-buttons-container, .ReactCrop, .ReactCrop__image, .ReactCrop__crop-selection, .image-upload-img";
				console.log(typeof document.body.onclick);
				if (!e.target.matches(activeBlock)) {
					console.log(e);
					console.log(e.target.matches(activeBlock));
					this.props.onClose(false);
				}
			});
	}

	onCropChange = crop => {
		this.setState({ crop });
	};

	onDrop(acceptedFiles, rejectedFiles) {
		if (Object.keys(rejectedFiles).length !== 0) {
			let text = document.getElementById("invite-text");
			text.textContent = "Only .png and .jpg are supported";
		} else {
			var blobPromise = new Promise((resolve, reject) => {
				const reader = new window.FileReader();
				reader.readAsDataURL(acceptedFiles[0]);
				reader.onloadend = () => {
					const base64data = reader.result;
					this.setState({
						files: acceptedFiles,
						src: base64data
					});
					resolve(base64data);
				};
			});

			blobPromise.then(value => {
				console.log(value);
			});
		}
	}

	handleCancelUploadingNewAvatarImageClick() {
		this.setState({
			files: []
		});
		this.props.onClose(false);
	}

	render() {
		if (this.state.files.length != 0) {
			return (
				<div className="popup-container">
					<div className="popup-container-header">
						<p>Crop new photo</p>
						<img
							src={require("./Resources/close-popup-image.png")}
							onClick={this.props.onClose}
							className="close-popup-img"
						/>
					</div>
					<div className="popup-body">
						<ReactCrop
							className="crop-container"
							src={this.state.src}
							crop={this.state.crop}
							onImageLoaded={this.onImageLoaded}
							onComplete={this.props.onCropComplete}
							onChange={this.onCropChange}
						/>
						<div className="crop-buttons-container">
							<button
								className="submit-btn"
								onClick={this.props.onUploadImageClick}
							>
								Crop and upload
							</button>
							<button
								className="cancel-submit-btn"
								onClick={
									this
										.handleCancelUploadingNewAvatarImageClick
								}
							>
								Cancel crop
							</button>
						</div>
					</div>
				</div>
			);
		}
		return (
			<Dropzone
				onDrop={this.onDrop}
				noKeyboard={true}
				accept={"image/jpeg, image/png"}
			>
				{({ getRootProps, getInputProps, isDragActive }) =>
					isDragActive ? (
						<div className="popup-container">
							<div className="popup-container-header">
								<p>Upload new photo</p>
								<img
									src={require("./Resources/close-popup-image.png")}
									onClick={this.props.onClose}
									className="close-popup-img"
								/>
							</div>
							<div
								className="popup-body-active"
								{...getRootProps()}
							>
								<input
									className="testinput"
									{...getInputProps()}
								/>
								<img
									src={require("./Resources/upload-image.png")}
									className="image-upload-img"
								/>
								<p
									style={{
										color: "rgb(93, 180, 235)",
										"font-family": "Verdana"
									}}
									id="invite-text"
								>
									Chouse a{" "}
									<b className="file-manualy-upload">file</b>{" "}
									or drag it here
								</p>
							</div>
						</div>
					) : (
						<div className="popup-container">
							<div className="popup-container-header">
								<p>Upload new photo</p>
								<img
									src={require("./Resources/close-popup-image.png")}
									onClick={this.props.onClose}
									className="close-popup-img"
								/>
							</div>
							<div className="popup-body" {...getRootProps()}>
								<input
									className="testinput"
									{...getInputProps()}
								/>
								<img
									src={require("./Resources/upload-image.png")}
									className="image-upload-img"
								/>
								<p
									style={{ "font-family": "Verdana" }}
									id="invite-text"
								>
									Chouse a{" "}
									<b className="file-manualy-upload">file</b>{" "}
									or drag it here
								</p>
							</div>
						</div>
					)
				}
			</Dropzone>
		);
	}
}

export default ImageUpload;
