﻿using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FarmHouse.Api.Controllers.Profile;
using FarmHouse.Library.Extencions;
using FarmHouse.Logic.FileLogic;
using FarmHouse.Logic.UsersLogic.Profile;
using FarmHouse.Models.ViewModels.HttpResults;
using FarmHouse.Models.ViewModels.JSON;
using FarmHouse.Models.ViewModels.Users.Profile;
using Microsoft.AspNetCore.Http;
using Moq;
using NUnit.Framework;

namespace FarmHouse.Api.Tests.Controllers.Profile
{
    [TestFixture]
    internal sealed class ProfileControllerTests
    {
        private Mock<IProfileLogic> _profileLogicMock;

        private Mock<IImageLogic> _imageLogicMock;

        private ProfileController _profileController;

        [SetUp]
        public void SetUp()
        {
            _profileLogicMock = new Mock<IProfileLogic>();

            _imageLogicMock = new Mock<IImageLogic>();

            _profileController = new ProfileController(_profileLogicMock.Object, _imageLogicMock.Object);
        }

        [Test]
        public void AboutUser_CallsGetUserAbout_OfProfileLogicOnce_WithAnyParams()
        {
            // Arrange
            var expectedResult = new DataHttpResult<string>(It.IsAny<string>());

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserAbout(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.AboutUser(It.IsAny<int>());

            // Assert
            _profileLogicMock.Verify(i => i.GetUserAbout(It.IsAny<int>()), Times.Once());
        }

        [TestCase(5)]
        [TestCase(-1)]
        [TestCase(2314)]
        [TestCase(816)]
        public void AboutUser_CallsGetUserAbout_OfProfileLogicOnce(int id)
        {
            // Arrange
            var expectedResult = new DataHttpResult<string>(It.IsAny<string>());

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserAbout(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.AboutUser(id);

            // Assert
            _profileLogicMock.Verify(i => i.GetUserAbout(It.Is<int>(x => x.Equals(id))), Times.Once());
        }

        [Test]
        public void AboutUser_ShouldReturnsCorrectResult()
        {
            // Arrange
            var testString = "Test string";
            var testError = "Test error";
            var statusCode = HttpStatusCode.Created.ToInt();

            var expectedResult = new DataHttpResult<string>(testString, testError, statusCode);

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserAbout(It.IsAny<int>()))
                .Returns(task);

            // Act
            Task<DataHttpResult<string>> actualResult = _profileController.AboutUser(It.IsAny<int>());

            // Assert
            Assert.IsNotNull(actualResult);

            Assert.IsInstanceOf(typeof(Task<DataHttpResult<string>>), actualResult);

            Assert.AreEqual(1, actualResult.Result.Errors.Count);

            Assert.AreEqual(testError, actualResult.Result.Errors.FirstOrDefault());

            Assert.AreEqual(testString, ((DataHttpResult<string>)actualResult.Result).Data);

            Assert.AreEqual(expectedResult.StatusCode, actualResult.Result.StatusCode);
        }

        [Test]
        public void UserCountry_CallsGetUserCountry_OfProfileLogicOnce_WithAnyParams()
        {
            // Arrange
            var expectedResult = new DataHttpResult<string>(It.IsAny<string>());

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserCountry(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UserCountry(It.IsAny<int>());

            // Assert
            _profileLogicMock.Verify(i => i.GetUserCountry(It.IsAny<int>()), Times.Once());
        }

        [TestCase(2)]
        [TestCase(-4)]
        [TestCase(2244)]
        [TestCase(12)]
        public void UserCountry_CallsGetUserCountry_OfProfileLogicOnce(int id)
        {
            // Arrange
            var expectedResult = new DataHttpResult<string>(It.IsAny<string>());

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserCountry(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UserCountry(id);

            // Assert
            _profileLogicMock.Verify(i => i.GetUserCountry(It.Is<int>(x => x.Equals(id))), Times.Once());
        }

        [Test]
        public void UserCountry_ShouldReturnsCorrectResult()
        {
            // Arrange
            var testString = "Test string";
            var testError = "Test error";
            var statusCode = HttpStatusCode.SeeOther.ToInt();

            var expectedResult = new DataHttpResult<string>(testString, testError, statusCode);

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserCountry(It.IsAny<int>()))
                .Returns(task);

            _profileLogicMock.Setup(x => x.GetUserCountry(It.IsAny<int>()))
                .Returns(task);

            // Act
            Task<DataHttpResult<string>> actualResult = _profileController.UserCountry(It.IsAny<int>());

            // Assert
            Assert.IsNotNull(actualResult);

            Assert.IsInstanceOf(typeof(Task<DataHttpResult<string>>), actualResult);

            Assert.AreEqual(1, actualResult.Result.Errors.Count);

            Assert.AreEqual(testError, actualResult.Result.Errors.FirstOrDefault());

            Assert.AreEqual(testString, ((DataHttpResult<string>)actualResult.Result).Data);

            Assert.AreEqual(expectedResult.StatusCode, actualResult.Result.StatusCode);
        }

        [Test]
        public void UserDateOfBirth_CallsGetUserBirthDate_OfProfileLogicOnce_WithAnyParams()
        {
            // Arrange
            var expectedResult = new DataHttpResult<string>(It.IsAny<string>());

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserBirthDate(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UserDateOfBirth(It.IsAny<int>());

            // Assert
            _profileLogicMock.Verify(i => i.GetUserBirthDate(It.IsAny<int>()), Times.Once());
        }

        [TestCase(12)]
        [TestCase(-23)]
        [TestCase(21233)]
        [TestCase(2)]
        public void UserDateOfBirth_CallsGetUserBirthDate_OfProfileLogicOnce(int id)
        {
            // Arrange
            var expectedResult = new DataHttpResult<string>(It.IsAny<string>());

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserBirthDate(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UserDateOfBirth(id);

            // Assert
            _profileLogicMock.Verify(i => i.GetUserBirthDate(It.Is<int>(x => x.Equals(id))), Times.Once());
        }

        [Test]
        public void UserDateOfBirth_ShouldReturnsCorrectResult()
        {
            // Arrange
            var testString = "09.04.2000";
            var testError = "Test error";
            var statusCode = HttpStatusCode.ServiceUnavailable.ToInt();

            var expectedResult = new DataHttpResult<string>(testString, testError, statusCode);

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserBirthDate(It.IsAny<int>()))
                .Returns(task);

            _profileLogicMock.Setup(x => x.GetUserBirthDate(It.IsAny<int>()))
                .Returns(task);

            // Act
            Task<DataHttpResult<string>> actualResult = _profileController.UserDateOfBirth(It.IsAny<int>());

            // Assert
            Assert.IsNotNull(actualResult);

            Assert.IsInstanceOf(typeof(Task<DataHttpResult<string>>), actualResult);

            Assert.AreEqual(1, actualResult.Result.Errors.Count);

            Assert.AreEqual(testError, actualResult.Result.Errors.FirstOrDefault());

            Assert.AreEqual(testString, ((DataHttpResult<string>)actualResult.Result).Data);

            Assert.AreEqual(expectedResult.StatusCode, actualResult.Result.StatusCode);
        }

        [Test]
        public void UserPhoneNumber_CallsGetUserPhone_OfProfileLogicOnce_WithAnyParams()
        {
            // Arrange
            var expectedResult = new DataHttpResult<string>(It.IsAny<string>());

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserPhone(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UserPhoneNumber(It.IsAny<int>());

            // Assert
            _profileLogicMock.Verify(i => i.GetUserPhone(It.IsAny<int>()), Times.Once());
        }

        [TestCase(23)]
        [TestCase(-5)]
        [TestCase(333234)]
        [TestCase(0)]
        public void UserPhoneNumber_CallsGetUserPhone_OfProfileLogicOnce(int id)
        {
            // Arrange
            var expectedResult = new DataHttpResult<string>(It.IsAny<string>());

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserPhone(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UserPhoneNumber(id);

            // Assert
            _profileLogicMock.Verify(i => i.GetUserPhone(It.Is<int>(x => x.Equals(id))), Times.Once());
        }

        [Test]
        public void UserPhoneNumber_ShouldReturnsCorrectResult()
        {
            // Arrange
            var testString = "+23433244";
            var testError = "Test error";
            var statusCode = HttpStatusCode.Accepted.ToInt();

            var expectedResult = new DataHttpResult<string>(testString, testError, statusCode);

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserPhone(It.IsAny<int>()))
                .Returns(task);

            // Act
            Task<DataHttpResult<string>> actualResult = _profileController.UserPhoneNumber(It.IsAny<int>());

            // Assert
            Assert.IsNotNull(actualResult);

            Assert.IsInstanceOf(typeof(Task<DataHttpResult<string>>), actualResult);

            Assert.AreEqual(1, actualResult.Result.Errors.Count);

            Assert.AreEqual(testError, actualResult.Result.Errors.FirstOrDefault());

            Assert.AreEqual(testString, ((DataHttpResult<string>)actualResult.Result).Data);

            Assert.AreEqual(expectedResult.StatusCode, actualResult.Result.StatusCode);
        }

        [Test]
        public void UserEmail_CallsGetUserEmail_OfProfileLogicOnce_WithAnyParams()
        {
            // Arrange
            var expectedResult = new DataHttpResult<string>(It.IsAny<string>());

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserEmail(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UserEmail(It.IsAny<int>());

            // Assert
            _profileLogicMock.Verify(i => i.GetUserEmail(It.IsAny<int>()), Times.Once());
        }

        [TestCase(2)]
        [TestCase(-234)]
        [TestCase(112345)]
        [TestCase(0)]
        public void UserEmail_CallsGetUserEmail_OfProfileLogicOnce(int id)
        {
            // Arrange
            var expectedResult = new DataHttpResult<string>(It.IsAny<string>());

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserEmail(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UserEmail(id);

            // Assert
            _profileLogicMock.Verify(i => i.GetUserEmail(It.Is<int>(x => x.Equals(id))), Times.Once());
        }

        [Test]
        public void UserEmail_ShouldReturnsCorrectResult()
        {
            // Arrange
            var testString = "vladislove@gmail.com";
            var testError = "Test error";
            var statusCode = HttpStatusCode.ResetContent.ToInt();

            var expectedResult = new DataHttpResult<string>(testString, testError, statusCode);

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserEmail(It.IsAny<int>()))
                .Returns(task);

            // Act
            Task<DataHttpResult<string>> actualResult = _profileController.UserEmail(It.IsAny<int>());

            // Assert
            Assert.IsNotNull(actualResult);

            Assert.IsInstanceOf(typeof(Task<DataHttpResult<string>>), actualResult);

            Assert.AreEqual(1, actualResult.Result.Errors.Count);

            Assert.AreEqual(testError, actualResult.Result.Errors.FirstOrDefault());

            Assert.AreEqual(testString, ((DataHttpResult<string>)actualResult.Result).Data);

            Assert.AreEqual(expectedResult.StatusCode, actualResult.Result.StatusCode);
        }

        [Test]
        public void UploadAvatarImage_CallsUploadNewAvatarImageAsync_OfImageLogicOnce_WithAnyParams()
        {
            // Arrange
            var task = Task<IHttpResult>.Factory.StartNew(() =>
            {
                return new HttpResult();
            });

            _imageLogicMock.Setup(x => x.UploadNewAvatarImageAsync(It.IsAny<string>(), It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UploadAvatarImage(new DocumentUploadModel());

            task.Wait();

            // Assert
            _imageLogicMock.Verify(i => i.UploadNewAvatarImageAsync(It.IsAny<string>(), It.IsAny<int>()), Times.Once());
        }

        [TestCase(2)]
        [TestCase(-234)]
        [TestCase(112345)]
        [TestCase(0)]
        [Test]
        public void UploadAvatarImage_CallsUploadNewAvatarImageAsync_OfImageLogicOnce(int id)
        {
            // Arrange
            var task = Task<IHttpResult>.Factory.StartNew(() =>
            {
                return new HttpResult();
            });

            _imageLogicMock.Setup(x => x.UploadNewAvatarImageAsync(It.IsAny<string>(), It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UploadAvatarImage(new DocumentUploadModel() {UserId = id} );

            task.Wait();

            // Assert
            _imageLogicMock.Verify(i => i.UploadNewAvatarImageAsync(It.IsAny<string>(), It.Is<int>(x => x.Equals(id))), Times.Once());
        }


        [Test]
        public void UploadAvatarImage_ShouldReturnsCorrectResult()
        {
            // Arrange
            var expectedError = "exp error";

            var expectedResult = new HttpResult(expectedError, HttpStatusCode.BadRequest.ToInt());

            var task = Task<IHttpResult>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _imageLogicMock.Setup(x => x.UploadNewAvatarImageAsync(It.IsAny<string>(), It.IsAny<int>()))
                .Returns(task);

            // Act
            var actualResult = _profileController.UploadAvatarImage(new DocumentUploadModel());

            task.Wait();

            // Assert
            Assert.IsNotNull(actualResult);

            Assert.IsInstanceOf(typeof(HttpResult), actualResult.Result);

            Assert.AreEqual(1, actualResult.Result.Errors.Count);

            Assert.AreEqual(expectedError, actualResult.Result.Errors.FirstOrDefault());

            Assert.AreEqual(expectedResult.StatusCode, actualResult.Result.StatusCode);
        }

        [Test]
        public void UserAvatar_CallsGetUserAvatarImage_OfProfileLogicOnce_WithAnyParams()
        {
            // Arrange
            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return new DataHttpResult<string>(It.IsAny<string>());
            });

            _profileLogicMock.Setup(x => x.GetUserAvatarImage(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UserAvatar(It.IsAny<int>());

            task.Wait();

            // Assert
            _profileLogicMock.Verify(i => i.GetUserAvatarImage(It.IsAny<int>()), Times.Once());
        }

        [TestCase(1)]
        [TestCase(-2234)]
        [TestCase(1345)]
        [TestCase(0)]
        [Test]
        public void UserAvatar_CallsGetUserAvatarImage_OfProfileOnce(int id)
        {
            // Arrange
            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return new DataHttpResult<string>(It.IsAny<string>());
            });

            _profileLogicMock.Setup(x => x.GetUserAvatarImage(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UserAvatar(id);

            task.Wait();

            // Assert
            _profileLogicMock.Verify(i => i.GetUserAvatarImage(It.Is<int>(x => x.Equals(id))), Times.Once());
        }


        [Test]
        public void UserAvatar_ShouldReturnsCorrectResult()
        {
            // Arrange
            var testString = "vladislove@gmail.com";
            var testError = "Test error";
            var statusCode = HttpStatusCode.ResetContent.ToInt();

            var expectedResult = new DataHttpResult<string>(testString, testError, statusCode);

            var task = Task<DataHttpResult<string>>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserAvatarImage(It.IsAny<int>()))
                .Returns(task);

            // Act
            var actualResult = _profileController.UserAvatar(It.IsAny<int>());

            task.Wait();

            // Assert
            Assert.IsNotNull(actualResult);

            Assert.IsInstanceOf(typeof(DataHttpResult<string>), actualResult.Result);

            Assert.AreEqual(1, actualResult.Result.Errors.Count);

            Assert.AreEqual(testError, actualResult.Result.Errors.FirstOrDefault());

            Assert.AreEqual(expectedResult.StatusCode, actualResult.Result.StatusCode);
        }

        [Test]
        public void UserFullName_CallsGetUserFullName_OfProfileLogicOnce_WithAnyParams()
        {
            // Arrange
            var task = Task<IHttpResult>.Factory.StartNew(() =>
            {
                return new HttpResult();
            });

            _profileLogicMock.Setup(x => x.GetUserFullName(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UserFullName(It.IsAny<int>());

            task.Wait();

            // Assert
            _profileLogicMock.Verify(i => i.GetUserFullName(It.IsAny<int>()), Times.Once());
        }

        [TestCase(1)]
        [TestCase(-223409)]
        [TestCase(13422355)]
        [TestCase(0)]
        public void UserFullName_CallsGetUserFullName_OfProfileOnce(int id)
        {
            // Arrange
            var task = Task<IHttpResult>.Factory.StartNew(() =>
            {
                return new HttpResult();
            });

            _profileLogicMock.Setup(x => x.GetUserFullName(It.IsAny<int>()))
                .Returns(task);

            // Act
            _profileController.UserFullName(id);

            task.Wait();

            // Assert
            _profileLogicMock.Verify(i => i.GetUserFullName(It.Is<int>(x => x.Equals(id))), Times.Once());
        }


        [Test]
        public void UserFullName_ShouldReturnsCorrectResult()
        {
            // Arrange
            var testJson = new JsonItem (new { name = "testName", surname = "tetsSurname" });
            var testError = "Test error";
            var statusCode = HttpStatusCode.ResetContent.ToInt();

            var expectedResult = new DataHttpResult<JsonItem>(testJson, testError, statusCode);

            var task = Task<IHttpResult>.Factory.StartNew(() =>
            {
                return expectedResult;
            });

            _profileLogicMock.Setup(x => x.GetUserFullName(It.IsAny<int>()))
                .Returns(task);

            // Act
            var actualResult = _profileController.UserFullName(It.IsAny<int>());

            task.Wait();

            // Assert
            Assert.IsNotNull(actualResult);

            Assert.IsInstanceOf(typeof(DataHttpResult<JsonItem>), actualResult.Result);

            Assert.AreEqual(1, actualResult.Result.Errors.Count);

            Assert.AreEqual(testError, actualResult.Result.Errors.FirstOrDefault());

            Assert.AreEqual(expectedResult.StatusCode, actualResult.Result.StatusCode);
        }
    }
}
