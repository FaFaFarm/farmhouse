﻿using System.Linq;
using System.Text;

namespace FarmHouse.Library.Extencions
{
    public static class StringExtencions
    {
        public static bool EndsWith(this string searchString, params string[] compareSubStrings)
        {
            return compareSubStrings.Any(x => searchString.EndsWith(x));
        }

        public static bool Is(this string searchString, params string[] compareStrings)
        {
            return compareStrings.Any(x => x == searchString);
        }

        public static StringBuilder Prepend(this StringBuilder stringBuilder, string stringToPrepend)
        {
            return stringBuilder.Insert(0, stringToPrepend);
        }
    }
}
