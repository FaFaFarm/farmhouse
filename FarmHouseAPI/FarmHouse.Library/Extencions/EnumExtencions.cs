﻿using System;

namespace FarmHouse.Library.Extencions
{
    public static class EnumExtencions
    {
        public static string EnumToString<T>(this T convertingEnumElement)
        {
            return Enum.GetName(typeof(T), convertingEnumElement);
        }

        public static int ToInt<T>(this T convertingEnumElement) where T : struct, IConvertible
        {
            return Convert.ToInt32(convertingEnumElement);
        }
    }
}
