﻿using System.Threading.Tasks;
using FarmHouse.Models.ViewModels.HttpResults;

namespace FarmHouse.Logic.UsersLogic
{
    public interface IUserLogic
    {
        Task<IHttpResult> GetUserByNameSubString(string substring, int currentUserId);
    }
}
