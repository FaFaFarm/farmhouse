﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FarmHouse.DataLayer.Users.Repositories;
using FarmHouse.Enums;
using FarmHouse.Logic.UsersLogic.Registration;
using FarmHouse.Models.DBModels.UsersModels;
using FarmHouse.Models.ViewModels.HttpResults;
using FarmHouse.Models.ViewModels.Users;

namespace FarmHouse.Logic.UsersLogic.Authentication
{
    public sealed class AuthenticationLogic : IAuthenticationLogic
    {
        private readonly IUserCredentialsRepository _userCredentialsRepository;

        private readonly IHashingLogic _hashingLogic;

        public AuthenticationLogic(IUserCredentialsRepository userCredentialsRepository, IHashingLogic hashingLogic)
        {
            _userCredentialsRepository = userCredentialsRepository ?? throw new NullReferenceException(nameof(userCredentialsRepository));

            _hashingLogic = hashingLogic ?? throw new NullReferenceException(nameof(hashingLogic));
        }

        public async Task<DataHttpResult<UserAuthInformation>> Authenticate(UserAuthenticationModel authenticationModel)
        {
            List<UserCredentials> usersWithAuthEmail = await _userCredentialsRepository.GetUsersByEmailAsync(authenticationModel.Email);

            if (usersWithAuthEmail.Any())
            {
                string savedHashPassword = usersWithAuthEmail.SingleOrDefault().HashPassword;

                bool isPasswordsMatch = _hashingLogic.CompareWithRfc2898Hash(authenticationModel.Password, savedHashPassword);
                
                if (isPasswordsMatch)
                {
                    var userAuthInformation = new UserAuthInformation
                    {
                        AccountType = (AccountTypes)usersWithAuthEmail.SingleOrDefault().AccountTypeId,
                        Id = usersWithAuthEmail.SingleOrDefault().Id
                    };

                    return new DataHttpResult<UserAuthInformation>(userAuthInformation);
                }
                else
                {
                    return new DataHttpResult<UserAuthInformation>(null, Resources.Global.Resources.IncorrectPasswordEntered, (int)HttpStatusCode.NotFound);
                }
            }

            return new DataHttpResult<UserAuthInformation>(null, Resources.Global.Resources.IncorrectEmailEntered, (int)HttpStatusCode.NotFound);
        }
    }
}
