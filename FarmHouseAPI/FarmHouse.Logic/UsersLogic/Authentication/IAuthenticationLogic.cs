﻿using System.Threading.Tasks;
using FarmHouse.Models.ViewModels.HttpResults;
using FarmHouse.Models.ViewModels.Users;

namespace FarmHouse.Logic.UsersLogic.Authentication
{
    public interface IAuthenticationLogic
    {
        Task<DataHttpResult<UserAuthInformation>> Authenticate(UserAuthenticationModel authenticationModel);
    }
}
