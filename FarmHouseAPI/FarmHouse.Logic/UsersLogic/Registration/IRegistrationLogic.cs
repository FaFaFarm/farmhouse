﻿using System.Threading.Tasks;
using FarmHouse.Models.ViewModels.Users;
using FarmHouse.Models.ViewModels.HttpResults;

namespace FarmHouse.Logic.UsersLogic.Registration
{
    public interface IRegistrationLogic
    {
        bool IsEmailTaken(string email);

        Task<IHttpResult> Register(UserRegisterModel registerModel);
    }
}
