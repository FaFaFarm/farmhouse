﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FarmHouse.DataLayer.Users.Repositories;
using FarmHouse.Enums;
using FarmHouse.Library.Extencions;
using FarmHouse.Models.DBModels.UsersModels;
using FarmHouse.Models.ViewModels.HttpResults;
using FarmHouse.Models.ViewModels.Users;

namespace FarmHouse.Logic.UsersLogic.Registration
{
    public sealed class RegistrationLogic : IRegistrationLogic
    {        
        private readonly IUserCredentialsRepository _userCredentialsRepository;

        private readonly IHashingLogic _hashingLogic;

        private readonly IUserInfoRepository _userInfoRepository;

        public RegistrationLogic(IUserCredentialsRepository userCredentialsRepository, IHashingLogic hashingLogic, IUserInfoRepository userInfoRepository)
        {
            _userCredentialsRepository = userCredentialsRepository ?? throw new NullReferenceException(nameof(userCredentialsRepository));

            _userInfoRepository = userInfoRepository ?? throw new NullReferenceException(nameof(_userInfoRepository));

            _hashingLogic = hashingLogic ?? throw new NullReferenceException(nameof(hashingLogic));
        }

        public bool IsEmailTaken(string email)
        {
            return _userCredentialsRepository.GetUsersByEmailAsync(email).Result.Any();
        }

        public async Task<IHttpResult> Register(UserRegisterModel registerModel)
        {
            if (IsEmailTaken(registerModel.Email))
            {
                return new HttpResult(Resources.Global.Resources.ThisEmailIsAlreadyTaken, HttpStatusCode.NotFound.ToInt());
            }

            string hashPassword = _hashingLogic.CreateRfc2898HashPassword(registerModel.Password);

            var newRecord = new UserCredentials
            {
                AccountTypeId = (int)(AccountTypes)Enum.Parse(typeof(AccountTypes), registerModel.AccountType),
                CountryId = registerModel.CountryId,
                CityId = registerModel.CityId,
                Email = registerModel.Email,
                HashPassword = hashPassword,
                Name = registerModel.Name,
                SecondName = registerModel.Surname,
                RegistrationDate = DateTime.Now
            };

            try
            {
                await _userCredentialsRepository.AddAsync(newRecord);
            }
            catch (Exception ex)
            {
                return new HttpResult(Resources.Global.Resources.UnexpectedServerError + ": " + ex.Message, HttpStatusCode.InternalServerError.ToInt());
            }

            return new HttpResult();
        }
    }
}
