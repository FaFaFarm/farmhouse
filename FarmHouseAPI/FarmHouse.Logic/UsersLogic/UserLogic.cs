﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FarmHouse.DataLayer.Users.Repositories;
using FarmHouse.Library.Extencions;
using FarmHouse.Models.ViewModels.HttpResults;
using FarmHouse.Models.ViewModels.Users;

namespace FarmHouse.Logic.UsersLogic
{
    public class UserLogic : IUserLogic
    {
        private readonly IUserInfoRepository _userInfoRepository;

        public UserLogic(IUserCredentialsRepository userCredentialsRepository, IUserInfoRepository userInfoRepository)
        {
            _userInfoRepository = userInfoRepository ?? throw new NullReferenceException(nameof(userInfoRepository));
        }

        public async Task<IHttpResult> GetUserByNameSubString(string substring, int currentUserId)
        {
            List<UserSearchModel> users = await _userInfoRepository.GetUsersByFullNameSubStringAsync(substring);

            if (users.IsNullOrEmpty())
            {
                return new HttpResult(Resources.Global.Resources.UnknownUser, (int)HttpStatusCode.NotFound);
            }

            return new DataHttpResult<List<UserSearchModel>>(users.Where(x => x.UserId != currentUserId).ToList());
        }
    }
}
