﻿using System.Threading.Tasks;
using FarmHouse.Models.ViewModels.HttpResults;

namespace FarmHouse.Logic.UsersLogic.Profile
{
    public interface IProfileLogic
    {
        Task<DataHttpResult<string>> GetUserCountry(int id);

        Task<DataHttpResult<string>> GetUserCity(int id);

        Task<DataHttpResult<string>> GetUserAbout(int id);

        Task<DataHttpResult<string>> GetUserBirthDate(int id);

        Task<DataHttpResult<string>> GetUserPhone(int id);

        Task<DataHttpResult<string>> GetUserEmail(int id);

        Task<DataHttpResult<string>> GetUserAvatarImage(int id);

        Task<IHttpResult> GetUserFullName(int id);
    }
}
