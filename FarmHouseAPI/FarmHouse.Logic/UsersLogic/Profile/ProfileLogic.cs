﻿using System;
using System.Net;
using System.Threading.Tasks;
using FarmHouse.DataLayer.Users.Repositories;
using FarmHouse.DataLayer.World.Repositories;
using FarmHouse.Models.DBModels.UsersModels;
using FarmHouse.Models.ViewModels.HttpResults;
using FarmHouse.Models.ViewModels.JSON;

namespace FarmHouse.Logic.UsersLogic.Profile
{
    public class ProfileLogic : IProfileLogic
    {
        private readonly IUserCredentialsRepository _userCredentialsRepository;

        private readonly ICountryRepository _countryRepository;

        private readonly ICityRepository _cityRepository;

        public ProfileLogic(IUserCredentialsRepository userCredentialsRepository, ICityRepository cityRepository, ICountryRepository countryRepository)
        {
            _userCredentialsRepository = userCredentialsRepository ?? throw new NullReferenceException(nameof(userCredentialsRepository));

            _countryRepository = countryRepository ?? throw new NullReferenceException(nameof(countryRepository));

            _cityRepository = cityRepository ?? throw new NullReferenceException(nameof(cityRepository));
        }

        public async Task<DataHttpResult<string>> GetUserCountry(int id)
        {
            UserCredentials user = await _userCredentialsRepository.GetByIdAsync(id);

            if (user == null)
            {
                return new DataHttpResult<string>(null, Resources.Global.Resources.UnknownUser, (int)HttpStatusCode.NotFound);
            }

            return new DataHttpResult<string>(_countryRepository.GetById(user.CountryId).CountryName);
        }

        public async Task<DataHttpResult<string>> GetUserCity(int id)
        {
            UserCredentials user = await _userCredentialsRepository.GetByIdAsync(id);

            if (user == null)
            {
                return new DataHttpResult<string>(null, Resources.Global.Resources.UnknownUser, (int)HttpStatusCode.NotFound);
            }

            return new DataHttpResult<string>(_cityRepository.GetById(user.CityId).CityName);
        }

        public async Task<DataHttpResult<string>> GetUserAbout(int id)
        {
            UserCredentials user = await _userCredentialsRepository.GetByIdAsync(id);

            if (user?.AdditionalUserInfo?.About == null)
            {
                return new DataHttpResult<string>(null, Resources.Global.Resources.UnknownInformation, (int)HttpStatusCode.NotFound);
            }

            return new DataHttpResult<string>(user.AdditionalUserInfo.About);
        }

        public async Task<DataHttpResult<string>> GetUserBirthDate(int id)
        {
            UserCredentials user = await _userCredentialsRepository.GetByIdAsync(id);

            if (user == null)
            {
                return new DataHttpResult<string>(null, Resources.Global.Resources.UnknownUser, (int)HttpStatusCode.NotFound);
            }

            var date = _userCredentialsRepository.GetById(id).AdditionalUserInfo.BirthdayDate.ToString();

            if (date == null)
            {
                return new DataHttpResult<string>(null, Resources.Global.Resources.UnknownInformation, (int)HttpStatusCode.NotFound);
            }

            return new DataHttpResult<string>(date);
        }

        public async Task<DataHttpResult<string>> GetUserPhone(int id)
        {
            UserCredentials user = await _userCredentialsRepository.GetByIdAsync(id);

            if (user == null)
            {
                return new DataHttpResult<string>(null, Resources.Global.Resources.UnknownUser, (int)HttpStatusCode.NotFound);
            }

            var phone = _userCredentialsRepository.GetById(id)?.AdditionalUserInfo.MobilePhoneNumber;

            if (phone == null)
            {
                return new DataHttpResult<string>(null, Resources.Global.Resources.UnknownInformation, (int)HttpStatusCode.NotFound);
            }

            return new DataHttpResult<string>(phone);
        }

        public async Task<DataHttpResult<string>> GetUserEmail(int id)
        {
            UserCredentials user = await _userCredentialsRepository.GetByIdAsync(id);

            if (user == null)
            {
                return new DataHttpResult<string>(null, Resources.Global.Resources.UnknownUser, (int)HttpStatusCode.NotFound);
            }

            return new DataHttpResult<string>(user.Email);
        }

        public async Task<DataHttpResult<string>> GetUserAvatarImage(int id)
        {
            UserCredentials user = await _userCredentialsRepository.GetByIdAsync(id);

            if (user == null)
            {
                return new DataHttpResult<string>(null, Resources.Global.Resources.UnknownUser, (int)HttpStatusCode.NotFound);
            }

            return new DataHttpResult<string>(user.AdditionalUserInfo.AvatarImage);
        }

        public async Task<IHttpResult> GetUserFullName(int id)
        {
            UserCredentials user = await _userCredentialsRepository.GetByIdAsync(id);

            if (user == null)
            {
                return new HttpResult(Resources.Global.Resources.UnknownUser, (int)HttpStatusCode.NotFound);
            }

            return new DataHttpResult<JsonItem>(new JsonItem(new {name = user.Name, surname = user.SecondName}));
        }
    }
}
