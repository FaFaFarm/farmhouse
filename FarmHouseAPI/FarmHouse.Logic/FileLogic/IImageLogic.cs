﻿using System.Threading.Tasks;
using FarmHouse.Models.ViewModels.HttpResults;

namespace FarmHouse.Logic.FileLogic
{
    public interface IImageLogic
    {
        Task<IHttpResult> UploadNewAvatarImageAsync(string imageBase64, int userId);
    }
}
