﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FarmHouse.DataLayer.Users.Repositories;
using FarmHouse.Enums;
using FarmHouse.Library.Extencions;
using FarmHouse.Models.DBModels.UsersModels;
using FarmHouse.Models.ViewModels.HttpResults;

namespace FarmHouse.Logic.FileLogic
{
    public sealed class ImageLogic : IImageLogic
    {
        private readonly IUserInfoRepository _userInfoRepository;

        private readonly IUserCredentialsRepository _userCredentialsRepository;

        public ImageLogic(IUserInfoRepository userInfoRepository, IUserCredentialsRepository userCredentialsRepository)
        {
            _userInfoRepository = userInfoRepository ?? throw new NullReferenceException();

            _userCredentialsRepository = userCredentialsRepository ?? throw new NullReferenceException();
        }

        public async Task<IHttpResult> UploadNewAvatarImageAsync(string imageBase64, int userId)
        {
            if (String.IsNullOrEmpty(imageBase64))
            {
                return new HttpResult(Resources.Global.Resources.ImageIsEmpty, HttpStatusCode.Forbidden.ToInt());
            }

            string imageFormat = Regex.Match(imageBase64, @"data:image/(.+?);base64").Groups[1].Value;
      
            byte[] image64 = Convert.FromBase64String(imageBase64.Split(',')[1]);            

            // TODO: Do not hardcode image formats
            if (imageFormat.Is("png", "jpg", "jpeg"))
            {
                using (var memoryStream = new MemoryStream(image64))
                {
                    if (memoryStream.Length < FileEnum.MaxImageSize.ToInt())
                    {
                        byte[] dropMetadataImage = (PatchAwayExif(memoryStream) as MemoryStream).ToArray();

                        AdditionalUserInfo currentUserInfo = _userCredentialsRepository.GetById(userId).AdditionalUserInfo;

                        var dropMetadataBase64Image = new StringBuilder(Convert.ToBase64String(dropMetadataImage, 0, dropMetadataImage.Length));
                        dropMetadataBase64Image.Prepend(@$"data:image/{imageFormat};base64,");

                        currentUserInfo.AvatarImage = dropMetadataBase64Image.ToString();

                        _userInfoRepository.Edit(currentUserInfo);

                        return new HttpResult();
                    }
                    else
                    {
                        return new HttpResult(Resources.Global.Resources.FileSizeIsTooLarge, HttpStatusCode.Forbidden.ToInt());
                    }
                }
            }
            else
            {
                return new HttpResult(Resources.Global.Resources.UnsupportedImageFormat, HttpStatusCode.Forbidden.ToInt());
            }
        }

        private Stream PatchAwayExif(Stream inStream)
        {
            var outStream = new MemoryStream();
            var jpegHeader = new byte[2];
            jpegHeader[0] = (byte)inStream.ReadByte();
            jpegHeader[1] = (byte)inStream.ReadByte();

            if (jpegHeader[0] == 0xff && jpegHeader[1] == 0xd8)
            {
                SkipAppHeaderSection(inStream);
            }

            outStream.WriteByte(0xff);
            outStream.WriteByte(0xd8);

            int readCount;
            byte[] readBuffer = new byte[4096];

            while ((readCount = inStream.Read(readBuffer, 0, readBuffer.Length)) > 0)
            {
                outStream.Write(readBuffer, 0, readCount);
            }                

            return outStream;
        }

        private void SkipAppHeaderSection(Stream inStream)
        {
            var header = new byte[2];
            header[0] = (byte)inStream.ReadByte();
            header[1] = (byte)inStream.ReadByte();

            while (header[0] == 0xff && (header[1] >= 0xe0 && header[1] <= 0xef))
            {
                int exifLength = inStream.ReadByte();
                exifLength = exifLength << 8;
                exifLength |= inStream.ReadByte();

                for (int i = 0; i < exifLength - 2; i++)
                {
                    inStream.ReadByte();
                }

                header[0] = (byte)inStream.ReadByte();
                header[1] = (byte)inStream.ReadByte();
            }

            inStream.Position -= 2; //skip back two bytes
        }
    }
}
