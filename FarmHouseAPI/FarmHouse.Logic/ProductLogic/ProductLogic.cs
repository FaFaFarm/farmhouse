﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmHouse.DataLayer.Users.UnitOfWork;
using FarmHouse.Models.DBModels.StoreModels.Categories;
using FarmHouse.Models.ViewModels.HttpResults;
using FarmHouse.Models.ViewModels.JSON;

namespace FarmHouse.Logic.ProductLogic
{
    public sealed class ProductLogic : IProductLogic
    {
        private readonly IUnitOFWork _unitOFWork;

        public ProductLogic(IUnitOFWork unitOFWork)
        {
            _unitOFWork = unitOFWork ?? throw new NullReferenceException(nameof(unitOFWork));
        }

        public IEnumerable<JsonItem> GetAllProductCategories()
        {
            IEnumerable<GeneralCategory> generalCategories = _unitOFWork.GeneralCategoriesRepository.GetAll(x => x.ProductCategories);

            return generalCategories.Select(x => new JsonItem(
                    new
                    {
                        GeneralCategoryId = x.Id,
                        GeneralCategoryName = x.Name,
                        ProductCategories = x.ProductCategories
                    }
                ));
        }

        public async Task<IEnumerable<JsonItem>> GetAllProductCategories(int maxCount, int partitionNumber)
        {
            IEnumerable<GeneralCategory> generalCategories = _unitOFWork.GeneralCategoriesRepository.GetPartial(
                maxCount, 
                partitionNumber, 
                x => x.ProductCategories);

            return generalCategories.Select(x => new JsonItem(
                    new
                    {
                        GeneralCategoryId = x.Id,
                        GeneralCategoryName = x.Name,
                        ProductCategories = x.ProductCategories,
                        PartitionNumber = partitionNumber,
                        Count = maxCount
                    }
                ));
        }
    }
}
