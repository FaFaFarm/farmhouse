﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FarmHouse.Models.ViewModels.JSON;

namespace FarmHouse.Logic.ProductLogic
{
    public interface IProductLogic
    {
        IEnumerable<JsonItem> GetAllProductCategories();

        Task<IEnumerable<JsonItem>> GetAllProductCategories(int maxCount, int partitionNumber);
    }
}
