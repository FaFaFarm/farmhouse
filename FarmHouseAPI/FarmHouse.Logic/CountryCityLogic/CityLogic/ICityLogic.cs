﻿using System.Collections.Generic;
using FarmHouse.Models.ViewModels.CountryCity;

namespace FarmHouse.Logic.CountryCityLogic.CityLogic
{
    public interface ICityLogic
    {
        IEnumerable<CityViewModel> GetAllCities();

        IEnumerable<CityViewModel> GetCitiesBySubString(string searchString);

        IEnumerable<CityViewModel> GetCitiesBySubStringForCountry(string searchString, int countryId);

        IEnumerable<CityViewModel> GetCitiesForCountry(int countryId);
    }
}
