﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FarmHouse.DataLayer.World.Repositories;
using FarmHouse.Models.DBModels.WorldModels;
using FarmHouse.Models.ViewModels.CountryCity;

namespace FarmHouse.Logic.CountryCityLogic.CityLogic
{
    public sealed class CityLogic : ICityLogic
    {
        private readonly ICityRepository _cityRepository;
        private readonly IMapper _mapper;

        public CityLogic(ICityRepository cityRepository, IMapper mapper)
        {
            _cityRepository = cityRepository ?? throw new NullReferenceException(nameof(cityRepository));

            _mapper = mapper ?? throw new NullReferenceException(nameof(mapper));
        }

        public IEnumerable<CityViewModel> GetAllCities()
        {
            var cities = _cityRepository.GetAll().ToList() ?? new List<City>();

            return _mapper.Map<IEnumerable<CityViewModel>>(cities);
        }

        public IEnumerable<CityViewModel> GetCitiesBySubString(string searchString)
        {                       
            List<City> cities = _cityRepository.GetCitiesBySubStringAsync(searchString).Result ?? new List<City>();

            return _mapper.Map<IEnumerable<CityViewModel>>(cities);
        }

        public IEnumerable<CityViewModel> GetCitiesBySubStringForCountry(string searchString, int countryId)
        {
            List<City> cities = _cityRepository.GetCitiesByCountryAndSubStringAsync(searchString, countryId).Result ?? new List<City>();

            return _mapper.Map<IEnumerable<CityViewModel>>(cities);
        }

        public IEnumerable<CityViewModel> GetCitiesForCountry(int countryId)
        {
            List<City> cities = _cityRepository.GetCitiesByCountyAsync(countryId).Result ?? new List<City>();

            return _mapper.Map<IEnumerable<CityViewModel>>(cities);
        }
    }
}
