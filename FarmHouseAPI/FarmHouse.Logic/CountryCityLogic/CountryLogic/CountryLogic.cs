﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FarmHouse.DataLayer.World.Repositories;
using FarmHouse.Models.DBModels.WorldModels;
using FarmHouse.Models.ViewModels.CountryCity;

namespace FarmHouse.Logic.CountryCityLogic.CountryLogic
{
    public sealed class CountryLogic : ICountryLogic
    {
        private readonly ICountryRepository _countryRepository;
        private readonly IMapper _mapper;

        public CountryLogic(ICountryRepository countryRepository, IMapper mapper)
        {
            _countryRepository = countryRepository ?? throw new NullReferenceException(nameof(countryRepository));

            _mapper = mapper ?? throw new NullReferenceException(nameof(mapper));
        }

        public IEnumerable<CountryViewModel> GetAllCountries()
        {
            var countries = _countryRepository.GetAll().ToList() ?? new List<Country>();

            return _mapper.Map<IEnumerable<CountryViewModel>>(countries);
        }

        public IEnumerable<CountryViewModel> GetCountriesBySubString(string searchString)
        {
            List<Country> countries = _countryRepository.GetCountriesBySubStringAsync(searchString).Result ?? new List<Country>();

            return _mapper.Map<IEnumerable<CountryViewModel>>(countries);
        }
    }
}
