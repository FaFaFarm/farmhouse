﻿using System.Collections.Generic;
using FarmHouse.Models.ViewModels.CountryCity;

namespace FarmHouse.Logic.CountryCityLogic.CountryLogic
{
    public interface ICountryLogic
    {
        IEnumerable<CountryViewModel> GetAllCountries();

        IEnumerable<CountryViewModel> GetCountriesBySubString(string searchString);
    }
}
