﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmHouse.DataLayer.Users.Repositories.Messages;
using FarmHouse.Models.DBModels.UsersModels.Messages;
using FarmHouse.Models.ViewModels.Users.Messages;

namespace FarmHouse.Logic.MessageLogic
{
    public class MessageLogic : IMessageLogic
    {
        private readonly IMessageRepository _messageRepository;

        public MessageLogic(IMessageRepository messageRepository)
        {
            _messageRepository = messageRepository ?? throw new NullReferenceException(nameof(messageRepository));
        }

        public async Task SaveTextUserMessage(TextMessage textMessage, int fromUserId)
        {
            DateTime sendMessageTime = DateTime.Now;

            if (!String.IsNullOrEmpty(textMessage.Message))
            {
                await _messageRepository.AddAsync(new MessageInformation
                {
                    Message = textMessage.Message,
                    FromUserId = fromUserId,
                    SendingDate = sendMessageTime,
                    ToUserId = textMessage.ToUserId,
                    WithAttachment = false
                });
            }
        }

        public async Task<List<List<ConversationMessageItem>>> GetDialogsForUser(int userId, int dialogsLimit = 0, int skipCount = 0)
        {
            if (userId != 0)
            {
                List<MessageInformation> allMessagesForCurrentUser = await _messageRepository.GetMessagesForCurrentUser(userId);

                IEnumerable<IGrouping<int, MessageInformation>> messagesGroupedBySenders = allMessagesForCurrentUser.GroupBy(x => x.FromUserId);

                var dialogs = new List<List<ConversationMessageItem>>();

                foreach (IGrouping<int, MessageInformation> group in messagesGroupedBySenders)
                {
                    int fromUserId = group.Key;

                    IEnumerable<MessageInformation> messagesToUser = allMessagesForCurrentUser.Where(x => x.ToUserId == fromUserId || x.FromUserId == fromUserId);
                    var currentDialog = new List<ConversationMessageItem>();

                    foreach (MessageInformation message in messagesToUser)
                    {
                        currentDialog.Add(new ConversationMessageItem
                        {
                            // TODO: Add attachments and IsRead.
                            InterlocutorUserId = message.FromUserId == userId ? message.ToUserId : message.FromUserId,
                            IsReceivedByCurrentUser = message.FromUserId == userId,
                            Message = message.Message,
                            WithAttachment = message.WithAttachment,
                            ReceivedDate = message.SendingDate
                        });
                    }

                    dialogs.Add(currentDialog.OrderBy(x => x.ReceivedDate).ToList());
                }

                if (dialogsLimit > 0)
                {
                    return dialogs.TakeLast(dialogsLimit).ToList();
                }
                else
                {
                    if (skipCount > 0)
                    {
                        return dialogs.TakeLast(dialogsLimit).Skip(skipCount).ToList();
                    }

                    return dialogs;
                }
            }

            // TODO: Return bad http reult.
            return null;
        }
    }
}
