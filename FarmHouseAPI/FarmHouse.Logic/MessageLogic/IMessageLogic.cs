﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FarmHouse.Models.ViewModels.Users.Messages;

namespace FarmHouse.Logic.MessageLogic
{
    public interface IMessageLogic
    {
        Task SaveTextUserMessage(TextMessage textMessage, int fromUserId);

        Task<List<List<ConversationMessageItem>>> GetDialogsForUser(int userId, int dialogsLimit = 0, int skipCount = 0);        
    }
}
