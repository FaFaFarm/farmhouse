﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace FarmHouse.Api.Middleware
{
    internal sealed class SecureJwtMiddleware
    {
        public IConfiguration AppConfiguration { get; set; }

        private readonly RequestDelegate _next;
        
        public SecureJwtMiddleware(RequestDelegate next, IConfiguration config)
        {
            _next = next;
            AppConfiguration = config;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var token = context.Request.Cookies[AppConfiguration["Coockies:AuthenticationToken"]];

            if (!string.IsNullOrEmpty(token))
            {
                context.Request.Headers.Add("Authorization", "Bearer " + token);
            }

            context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
            context.Response.Headers.Add("X-Xss-Protection", "1");
            context.Response.Headers.Add("X-Frame-Options", "DENY");

            await _next(context);
        }
    }
}
