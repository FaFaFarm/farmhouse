﻿using System;
using System.Threading.Tasks;
using FarmHouse.Core.Logger;
using FarmHouse.Core.Logger.Extencions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace FarmHouse.Api.Middleware
{
    internal sealed class LogMiddleware
    {
        private RequestDelegate _requestDelegate;
        private ILogProvider _logProvider;
        private IConfiguration _configuration;

        public LogMiddleware(RequestDelegate requestDelegate, ILogProvider logProvider, IConfiguration configuration)
        {
            _requestDelegate = requestDelegate ?? throw new ArgumentNullException(nameof(requestDelegate));
            _logProvider = logProvider ?? throw new ArgumentNullException(nameof(requestDelegate));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));

            Setup();
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _requestDelegate.Invoke(context);
            }
            catch (Exception ex)
            {
                _logProvider.LogError(ex.Message);
                throw;
            }
        }

        private void Setup()
        {
            _logProvider.AddFileLogger(_configuration["LogFilePath"]);
            _logProvider.AddConsoleLogger();
        }
    }
}
