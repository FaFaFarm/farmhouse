﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace FarmHouse.Api.Middleware
{
    internal sealed class UrlRewritingMiddleware
    {
        public IConfiguration AppConfiguration { get; set; }

        private readonly RequestDelegate _next;

        public UrlRewritingMiddleware(RequestDelegate next, IConfiguration config)
        {
            _next = next;
            AppConfiguration = config;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.Request.Path.Value.EndsWith("/"))
            {
                var uiUri = @"http://localhost:3000";
                context.Response.Redirect(uiUri);
            }

            await _next(context);
        }
    }
}
