using FarmHouse.Logic.CountryCityLogic.CityLogic;
using FarmHouse.Logic.CountryCityLogic.CountryLogic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using FarmHouse.DataLayer.World.Contexts;
using FarmHouse.DataLayer.World.Repositories;
using AutoMapper;
using FarmHouse.Core.Logger;
using FarmHouse.Core.Mapper;
using FarmHouse.Logic.UsersLogic.Registration;
using FarmHouse.DataLayer.Users.Repositories;
using FarmHouse.DataLayer.Users.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.CookiePolicy;
using FarmHouse.Logic.UsersLogic.Authentication;
using FarmHouse.Logic.UsersLogic.Profile;
using FarmHouse.Models.Authentication;
using FarmHouse.Api.Extencions;
using FarmHouse.Api.Middleware;
using FarmHouse.Api.Hubs;
using FarmHouse.DataLayer.Users.Repositories.Messages;
using FarmHouse.Logic.MessageLogic;
using FarmHouse.Logic.UsersLogic;
using FarmHouse.Logic.FileLogic;
using FarmHouse.DataLayer.Users.UnitOfWork;
using FarmHouse.Logic.ProductLogic;

namespace FarmHouse.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public IHostingEnvironment Environment { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Environment = environment;
            var configBuilder = new ConfigurationBuilder();

            if (environment.IsDevelopment())
            {
                configBuilder.AddJsonFile("appsettings.json");
            }
            else
            {
                configBuilder.AddJsonFile("release.json");
            }

            Configuration = configBuilder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSignalR();

            services.ConfigureFluentValidator();

            #region DbContexts

            services.AddDbContext<WorldContext>(op => op.UseNpgsql(Configuration["ConnectionStrings:WorldDBConnectionString"]));

            services.AddDbContext<UsersContext>(op => op.UseNpgsql(Configuration["ConnectionStrings:UserDBConnectionString"]));

            #endregion

            #region Authentication

            var section = Configuration.GetSection("AuthenticationOptions");
            var options = section.Get<AuthenticationOptions>();
            var jwtOptions = new JwtOptions(options.Audience, options.Issuer, options.SecretKey, options.Lifetime);
            services.AddApiJwtAuthentication(jwtOptions, Configuration);

            #endregion

            #region Mapper

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            #endregion

            #region Repositories

            services.AddScoped<ICityRepository, CityRepository>();

            services.AddScoped<ICountryRepository, CountryRepository>();

            services.AddScoped<IUserCredentialsRepository, UserCredentialsRepository>();
            
            services.AddScoped<IUserOccupationRepository, UserOccupationRepository>();

            services.AddScoped<IMessageRepository, MessageRepository>();

            services.AddScoped<IUserInfoRepository, UserInfoRepository>();

            services.AddScoped<IUnitOFWork, UnitOfWork>();

            #endregion

            #region Logics

            services.AddScoped<ICountryLogic, CountryLogic>();

            services.AddScoped<ICityLogic, CityLogic>();
            
            services.AddScoped<IRegistrationLogic, RegistrationLogic>(); 

            services.AddScoped<IAuthenticationLogic, AuthenticationLogic>();

            services.AddScoped<IHashingLogic, HashingLogic>();

            services.AddScoped<IProfileLogic, ProfileLogic>();
            
            services.AddScoped<IMessageLogic, MessageLogic>();

            services.AddScoped<IUserLogic, UserLogic>();

            services.AddScoped<IImageLogic, ImageLogic>();

            services.AddScoped<IProductLogic, ProductLogic>();

            #endregion

            #region Loggers

            services.AddSingleton<ILogProvider, LogProvider>();

            #endregion

            services.AddSingleton<IConfiguration>(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseMiddleware<LogMiddleware>();

            app.UseMiddleware<SecureJwtMiddleware>();

            app.UseCors(x => x
            .WithOrigins("http://localhost:3000") //SPA client
            .AllowCredentials()
            .AllowAnyMethod()
            .AllowAnyHeader());

            app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseRouting();

            app.UseCookiePolicy(new CookiePolicyOptions
            {
                MinimumSameSitePolicy = SameSiteMode.Strict,
                HttpOnly = HttpOnlyPolicy.Always,
                Secure = CookieSecurePolicy.Always
            });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

                endpoints.MapHub<MessageHub>("/chat");
            });

            app.Run(async (context) =>
            {
                if (env.IsDevelopment())
                {
                    await context.Response.WriteAsync("Farm House inc");
                }
                else
                {
                    await context.Response.WriteAsync("Farm House inc (Release)");
                }
            });
        }
    }
}
