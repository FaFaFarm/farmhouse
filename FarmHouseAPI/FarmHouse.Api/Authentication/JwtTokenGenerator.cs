﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using FarmHouse.Models.Authentication;
using Microsoft.IdentityModel.Tokens;

namespace FarmHouse.Api.Authentication
{
    public sealed class JwtTokenGenerator : IJwtTokenGenerator
    {
        private readonly JwtOptions _jwtOptions;

        public JwtTokenGenerator(JwtOptions jwtOptions)
        {
            _jwtOptions = jwtOptions ?? throw new NullReferenceException(nameof(jwtOptions));
        }

        public JwtTokenResult GenerateJwtToken(List<Claim> claims)
        {
            TimeSpan expiration = TimeSpan.FromMinutes(_jwtOptions.TokenExpiryInMinutes);

            var jwtToken = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.Audience,
                claims: claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.Add(expiration),
                signingCredentials: new SigningCredentials(_jwtOptions.SecurityKey, SecurityAlgorithms.HmacSha256Signature)); 

            var accessToken = new JwtSecurityTokenHandler().WriteToken(jwtToken);

            return new JwtTokenResult
            {
                AccessToken = accessToken,
                Expires = expiration
            };
        }
    }
}
