﻿using System.Collections.Generic;
using System.Security.Claims;
using FarmHouse.Models.Authentication;

namespace FarmHouse.Api.Authentication
{
    public interface IJwtTokenGenerator
    {
        JwtTokenResult GenerateJwtToken(List<Claim> claims);
    }
}
