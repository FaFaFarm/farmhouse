﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Authorization;
using FarmHouse.Models.ViewModels.Users.Messages;
using FarmHouse.Logic.MessageLogic;
using FarmHouse.Models.ViewModels.HttpResults;
using FarmHouse.Library.Extencions;

namespace FarmHouse.Api.Hubs
{
    [Authorize]
    public class MessageHub : Hub
    {
        private readonly IMessageLogic _messageLogic;

        public MessageHub(IMessageLogic messageLogic)
        {
            _messageLogic = messageLogic ?? throw new NullReferenceException(nameof(messageLogic));
        }

        public async Task SendTextMessageToUser(TextMessage textMessage)
        {
            int userId = 0;

            ClaimsPrincipal user = Context.User;
            
            if (!int.TryParse(user.FindFirst(ClaimTypes.Name)?.Value, out userId))
            {
                throw new HubException();
            }

            await _messageLogic.SaveTextUserMessage(textMessage, userId);

            List<string> connectionsForCurrentUser = HubManager.GetUserConnections(userId);

            List<string> connectionsForTargetUser = HubManager.GetUserConnections(textMessage.ToUserId);

            if (!connectionsForCurrentUser.IsNullOrEmpty())
            {
                foreach (string client in connectionsForCurrentUser)
                {
                    await Clients.Client(client).SendAsync("ReceiveTextMessage", textMessage.Message, true);
                }
            }

            if (!connectionsForTargetUser.IsNullOrEmpty())
            {
                foreach (string client in connectionsForTargetUser)
                {
                    await Clients.Client(client).SendAsync("ReceiveTextMessage", textMessage.Message, false);
                }
            }
        }

        public override async Task OnConnectedAsync()
        {
            int userId = 0;

            ClaimsPrincipal user = Context.User;

            if (!int.TryParse(user.FindFirst(ClaimTypes.Name)?.Value, out userId))
            {
                throw new HubException();
            }

            HubManager.AddConnection(Context.ConnectionId, userId);

            List<List<ConversationMessageItem>> dialogsForUser = await _messageLogic.GetDialogsForUser(userId);

            HubManager.AddConnection(Context.ConnectionId, userId);
            await base.OnConnectedAsync();

            if (dialogsForUser != null)
            {
                await Clients.Client(Context.ConnectionId).SendAsync("GetAllMessages", new DataHttpResult<List<List<ConversationMessageItem>>>(dialogsForUser));
            }
            else
            {
                await Clients.Client(Context.ConnectionId).SendAsync("GetAllMessages", new HttpResult(Resources.Global.Resources.UnknownUser, HttpStatusCode.NotFound.ToInt()));
            }
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            HubManager.RemoveConnection(Context.ConnectionId);
            return base.OnDisconnectedAsync(exception);
        }
    }
}
