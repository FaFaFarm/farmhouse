﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FarmHouse.Api.Hubs
{
    public class HubManager
    {
        private static readonly object Lock = new object();
        private static readonly Dictionary<int, List<string>> Connections = new Dictionary<int, List<string>>();

        public static void AddConnection(string clientId, int userId)
        {
            lock (Lock)
            {
                if (!String.IsNullOrWhiteSpace(clientId) && !Connections.Any(x => x.Value.Contains(clientId)))
                {
                    if (!Connections.ContainsKey(userId))
                    {
                        Connections.Add(userId, new List<string>{ clientId });
                    }
                    else
                    {
                        Connections[userId].Add(clientId);
                    }
                }
            }
        }

        public static void RemoveConnection(string connectionId)
        {
            lock (Lock)
            {
                if (!String.IsNullOrWhiteSpace(connectionId) && Connections.Any(x => x.Value.Contains(connectionId)))
                {
                    Connections.FirstOrDefault(x => x.Value.Contains(connectionId)).Value.Remove(connectionId);
                }
            }
        }

        public static List<string> GetUserConnections(int userId)
        {
            var userConnections = new List<string>();

            lock (Lock)
            {
                if (Connections != null && Connections.ContainsKey(userId))
                {
                    userConnections = Connections.Where(pair => pair.Key == userId)
                        .Select(pair => pair.Value).FirstOrDefault();
                }
            }

            return userConnections;
        }
    }
}
