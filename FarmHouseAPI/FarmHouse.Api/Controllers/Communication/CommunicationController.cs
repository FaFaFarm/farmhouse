﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using FarmHouse.Api.Controllers.Base;
using FarmHouse.Library.Extencions;
using FarmHouse.Logic.MessageLogic;
using FarmHouse.Models.ViewModels.HttpResults;
using FarmHouse.Models.ViewModels.Users.Messages;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FarmHouse.Api.Controllers.Communication
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    [ApiController]
    public class CommunicationController : BaseHttpResponse
    {
        private readonly IMessageLogic _messageLogic;

        public CommunicationController(IMessageLogic messageLogic)
        {
            _messageLogic = messageLogic ?? throw new NullReferenceException(nameof(messageLogic));
        }

        // api/Communication/GetDialogs/
        [HttpGet("{id}/{dialogsLimit}")]
        public async Task<IHttpResult> GetDialogs(int id, int dialogsLimit)
        {
            List<List<ConversationMessageItem>> dialogs = await _messageLogic.GetDialogsForUser(id, dialogsLimit);

            if (dialogs != null)
            {
                return new DataHttpResult<List<List<ConversationMessageItem>>>(dialogs);
            }
            else
            {
                return new HttpResult(Resources.Global.Resources.UnexpectedServerError, HttpStatusCode.InternalServerError.ToInt());
            }
        }

        // api/Communication/GetDialogs/
        [HttpGet("{id}")]
        public async Task<IHttpResult> GetDialogs(int id)
        {
            List<List<ConversationMessageItem>> dialogs = await _messageLogic.GetDialogsForUser(id);

            if (dialogs != null)
            {
                return new DataHttpResult<List<List<ConversationMessageItem>>>(dialogs);
            }
            else
            {
                return new HttpResult(Resources.Global.Resources.UnexpectedServerError, HttpStatusCode.InternalServerError.ToInt());
            }
        }

        // api/Communication/GetDialogs/
        [HttpGet("{id}/{dialogsLimit}/{skipCount}")]
        public async Task<IHttpResult> GetDialogs(int id, int dialogsLimit, int skipCount)
        {
            List<List<ConversationMessageItem>> dialogs = await _messageLogic.GetDialogsForUser(id, dialogsLimit, skipCount);

            if (dialogs != null)
            {
                return new DataHttpResult<List<List<ConversationMessageItem>>>(dialogs);
            }
            else
            {
                return new HttpResult(Resources.Global.Resources.UnexpectedServerError, HttpStatusCode.InternalServerError.ToInt());
            }
        }
    }
}
