﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using FarmHouse.Api.Authentication;
using FarmHouse.Api.Controllers.Base;
using FarmHouse.Library.Extencions;
using FarmHouse.Logic.UsersLogic.Authentication;
using FarmHouse.Models.Authentication;
using FarmHouse.Models.ViewModels.HttpResults;
using FarmHouse.Models.ViewModels.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace FarmHouse.Api.Controllers.Authentication
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public sealed class AuthenticationController : BaseHttpResponse
    {
        private readonly IAuthenticationLogic _authenticationLogic;

        private readonly IJwtTokenGenerator _jwtTokenGenerator;

        private readonly IConfiguration _configuration;

        public AuthenticationController(IAuthenticationLogic authenticationLogic, IJwtTokenGenerator jwtTokenGenerator, IConfiguration configuration)
        {
            _authenticationLogic = authenticationLogic ?? throw new NullReferenceException(nameof(authenticationLogic));

            _jwtTokenGenerator = jwtTokenGenerator ?? throw new NullReferenceException(nameof(jwtTokenGenerator));

            _configuration = configuration ?? throw new NullReferenceException(nameof(configuration));
        }

        // api/authentication/authenticate
        [HttpPost]
        public async Task<IHttpResult> Authenticate([FromBody] UserAuthenticationModel authenticationModel)
        {
            DataHttpResult<UserAuthInformation> httpDataResult = await _authenticationLogic.Authenticate(authenticationModel);

            if (httpDataResult.IsSuccessful)
            {
                var userClaims = new List<Claim>()
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, httpDataResult.Data.Id.ToString()),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, httpDataResult.Data.AccountType.EnumToString())
                };

                JwtTokenResult jwtTokenResult = _jwtTokenGenerator.GenerateJwtToken(userClaims);

                HttpContext.Response.Cookies.Append(
                    _configuration["Coockies:AuthenticationToken"],
                    jwtTokenResult.AccessToken,
                    new CookieOptions { MaxAge = TimeSpan.FromMinutes(Int32.Parse(_configuration["AuthenticationOptions:Lifetime"]))});
            }

            return httpDataResult;
        }
    }
}