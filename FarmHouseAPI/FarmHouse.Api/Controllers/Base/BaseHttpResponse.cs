﻿using FarmHouse.Models.ViewModels.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Text;

namespace FarmHouse.Api.Controllers.Base
{
    public class BaseHttpResponse : ControllerBase
    {
        protected ActionResult CreateOKHttpResponse(object data)
        {
            var result = new ObjectResult(data)
            {
                StatusCode = (int)HttpStatusCode.OK
            };

            Response.Headers.Clear();
            ConfigHeader();

            return result;
        }

        protected ContentResult CreateHtmlResponse(string htmlContent)
        {
            var result = new ContentResult()
            {
                ContentType = "text/html",
                Content = htmlContent,
                StatusCode = (int)HttpStatusCode.OK
            };

            Response.Headers.Clear();
            ConfigHeader();

            return result;
        }

        protected IEnumerable<T> CreateListOkHttpResponse<T>(IEnumerable<T> data)
        {
            Response.Headers.Clear();
            ConfigHeader();

            return data;
        }     
        
        private void ConfigHeader(bool allowCredentials = true, string methods = "GET, POST", string origin = "*", string contentType = "nosniff", string protection = "1", string frame = "DENY")
        {
            Response.Headers.Add("Access-Control-Allow-Credentials", allowCredentials.ToString());
            Response.Headers.Add("Access-Control-Allow-Methods", methods);
            Response.Headers.Add("Access-Control-Allow-Origin", origin);
            Response.Headers.Add("X-Content-Type-Options", contentType);
            Response.Headers.Add("X-Xss-Protection", protection);
            Response.Headers.Add("X-Frame-Options", frame);
        }
    }
}
