﻿using Microsoft.AspNetCore.Mvc;
using FarmHouse.Api.Controllers.Base;
using FarmHouse.Models.ViewModels.HtmlResults;

namespace FarmHouse.Api.Controllers.Resource
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public sealed class RegistrationPageController : BaseHttpResponse
    {
        // api/registrationpage/aboutcompanytext
        [HttpGet()]
        public ContentResult AboutCompanyText()
        {
            return CreateHtmlResponse(Resources.Global.Resources.AboutCompany);
        }

        // api/registrationpage/companymissiontext
        [HttpGet()]
        public ActionResult CompanyMissionText()
        {
            return CreateOKHttpResponse(new HtmlResult(Resources.Global.Resources.CompanyMission));
        }
    }
}
