﻿using System;
using System.Collections.Generic;
using System.Linq;
using FarmHouse.Api.Controllers.Base;
using FarmHouse.Logic.CountryCityLogic.CityLogic;
using FarmHouse.Models.ViewModels.CountryCity;
using Microsoft.AspNetCore.Mvc;

namespace FarmHouse.Api.Controllers.CountryCityControllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public sealed class CityController : BaseHttpResponse
    {
        private readonly ICityLogic _cityLogic;

        public CityController(ICityLogic cityLogic)
        {
            _cityLogic = cityLogic ?? throw new NullReferenceException(nameof(cityLogic));
        }

        // api/city/cities
        [HttpGet]
        public IEnumerable<CityViewModel> Cities()
        {
            return CreateListOkHttpResponse(_cityLogic.GetAllCities());
        }

        // api/city/citiesautocomplite
        [HttpGet("{search}")]
        public IEnumerable<CityViewModel> CitiesAutocomplite(string search)
        {
            return CreateListOkHttpResponse(_cityLogic.GetCitiesBySubString(search).ToList());
        }

        // api/city/citiesbycountry
        [HttpGet("{id}")]
        public IEnumerable<CityViewModel> CitiesByCountry(int id)
        {
            return CreateListOkHttpResponse(_cityLogic.GetCitiesForCountry(id).ToList());
        }

        // api/city/citiesbycountryautocomplite
        [HttpGet("{search}/{id}")]
        public IEnumerable<CityViewModel> CitiesByCountryAutocomplite(string search, int id)
        {
            return CreateListOkHttpResponse(_cityLogic.GetCitiesBySubStringForCountry(search, id).ToList());
        }
    }
}
