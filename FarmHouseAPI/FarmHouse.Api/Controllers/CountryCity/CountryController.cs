﻿using System;
using System.Collections.Generic;
using System.Linq;
using FarmHouse.Api.Controllers.Base;
using FarmHouse.Logic.CountryCityLogic.CountryLogic;
using FarmHouse.Models.ViewModels.CountryCity;
using Microsoft.AspNetCore.Mvc;

namespace FarmHouse.Api.Controllers.CountryCity
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public sealed class CountryController : BaseHttpResponse
    {
        private readonly ICountryLogic _countryLogic;

        public CountryController(ICountryLogic countryLogic)
        {
            _countryLogic = countryLogic ?? throw new NullReferenceException(nameof(countryLogic));
        }

        // api/country/countries
        [HttpGet()]
        public IEnumerable<CountryViewModel> Countries()
        {
            return CreateListOkHttpResponse(_countryLogic.GetAllCountries().ToList());
        }

        // api/country/CountriesAutocomplite
        [HttpGet("{search}")]
        public IEnumerable<CountryViewModel> CountriesAutocomplite(string search)
        {
            return CreateListOkHttpResponse(_countryLogic.GetCountriesBySubString(search).ToList());
        }
    }
}
