﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FarmHouse.Api.Controllers.Base;
using FarmHouse.Logic.ProductLogic;
using FarmHouse.Models.ViewModels.JSON;
using Microsoft.AspNetCore.Mvc;

namespace FarmHouse.Api.Controllers.Products
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductsController : BaseHttpResponse
    {
        private readonly IProductLogic _productLogic;

        public ProductsController(IProductLogic productLogic)
        {
            _productLogic = productLogic ?? throw new NullReferenceException(nameof(productLogic));
        }

        [HttpGet("{maxCount}/{partition}")]
        public async Task<IEnumerable<JsonItem>> ProductCategories(int maxCount, int partition)
        {
            return await _productLogic.GetAllProductCategories(maxCount, partition);
        }

        [HttpGet]
        public IEnumerable<JsonItem> ProductCategories()
        {
            return _productLogic.GetAllProductCategories();
        }
    }
}