﻿using System;
using System.Threading.Tasks;
using FarmHouse.Logic.UsersLogic.Registration;
using FarmHouse.Models.ViewModels.HttpResults;
using FarmHouse.Models.ViewModels.Users;
using Microsoft.AspNetCore.Mvc;

namespace FarmHouse.Api.Controllers.Registration
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public sealed class RegistrationController : ControllerBase
    {
        private readonly IRegistrationLogic _registrationLogic;

        public RegistrationController(IRegistrationLogic registrationLogic)
        {
            _registrationLogic = registrationLogic ?? throw new NullReferenceException(nameof(registrationLogic));
        }

        // api/registration/register
        [HttpPost]
        public async Task<IHttpResult> Register([FromBody] UserRegisterModel registerModel)
        {
            return await _registrationLogic.Register(registerModel);
        }
    }
}