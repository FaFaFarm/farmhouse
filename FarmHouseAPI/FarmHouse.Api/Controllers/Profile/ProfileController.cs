﻿using System;
using System.Threading.Tasks;
using FarmHouse.Api.Controllers.Base;
using FarmHouse.Logic.UsersLogic.Profile;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using FarmHouse.Models.ViewModels.HttpResults;
using FarmHouse.Logic.FileLogic;
using FarmHouse.Models.ViewModels.Users.Profile;

namespace FarmHouse.Api.Controllers.Profile
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    [ApiController]
    public class ProfileController : BaseHttpResponse
    {
        private readonly IProfileLogic _profileLogic;

        private readonly IImageLogic _imageLogic;

        public ProfileController(IProfileLogic profileLogic, IImageLogic imageLogic)
        {
            _profileLogic = profileLogic ?? throw new NullReferenceException(nameof(profileLogic));

            _imageLogic = imageLogic ?? throw new NullReferenceException(nameof(imageLogic));
        }

        // api/Profile/UserFullName
        [HttpGet("{id}")]
        public async Task<IHttpResult> UserFullName(int id)
        {
            return await _profileLogic.GetUserFullName(id);
        }

        // api/Profile/AboutUser
        [HttpGet("{id}")]
        public async Task<DataHttpResult<string>> AboutUser(int id)
        {
            return await _profileLogic.GetUserAbout(id);
        }

        // api/Profile/UserCountry
        [HttpGet("{id}")]
        public async Task<DataHttpResult<string>> UserCountry(int id)
        {
            return await _profileLogic.GetUserCountry(id);
        }

        // api/Profile/UserCity
        [HttpGet("{id}")]
        public async Task<DataHttpResult<string>> UserCity(int id)
        {
            return await _profileLogic.GetUserCity(id);
        }

        // api/Profile/UserDateOfBirth
        [HttpGet("{id}")]
        public async Task<DataHttpResult<string>> UserDateOfBirth(int id)
        {
            return await _profileLogic.GetUserBirthDate(id);
        }

        // api/Profile/UserPhoneNumber
        [HttpGet("{id}")]
        public async Task<DataHttpResult<string>> UserPhoneNumber(int id)
        {
            return await _profileLogic.GetUserPhone(id);
        }

        // api/Profile/UserEmail
        [HttpGet("{id}")]
        public async Task<DataHttpResult<string>> UserEmail(int id)
        {
            return await _profileLogic.GetUserEmail(id);
        }

        // api/Profile/UploadAvatarImage
        [HttpPost]
        public async Task<IHttpResult> UploadAvatarImage([FromBody]DocumentUploadModel image)
        {
            return await _imageLogic.UploadNewAvatarImageAsync(image.Document, image.UserId);
        }

        // api/Profile/UserAvatar
        [HttpGet("{id}")]
        public Task<DataHttpResult<string>> UserAvatar(int id)
        {
            return _profileLogic.GetUserAvatarImage(id);
        }
    }
}
