﻿using System;
using System.Threading.Tasks;
using FarmHouse.Api.Controllers.Base;
using FarmHouse.Logic.UsersLogic;
using FarmHouse.Models.ViewModels.HttpResults;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FarmHouse.Api.Controllers.Profile
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    [ApiController]
    public class SearchController : BaseHttpResponse
    {
        private readonly IUserLogic _userLogic;

        public SearchController(IUserLogic userLogic)
        {
            _userLogic = userLogic ?? throw new NullReferenceException(nameof(userLogic));
        }

        // api/Search/SearchUserBySubString
        [HttpGet("{id}/{search}")]
        public async Task<IHttpResult> SearchUserBySubString(string search, int id)
        {
            return await _userLogic.GetUserByNameSubString(search, id);
        }
    }
}
