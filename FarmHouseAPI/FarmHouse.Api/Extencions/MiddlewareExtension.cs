﻿using FarmHouse.Api.Middleware;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Builder;

namespace FarmHouse.Api.Extencions
{
    public static class MiddlewareExtension
    {
        public static IApplicationBuilder UseXsrfProtection(this IApplicationBuilder builder, IAntiforgery antiforgery)
            => builder.UseMiddleware<XsrfProtectionMiddleware>(antiforgery);
    }
}
