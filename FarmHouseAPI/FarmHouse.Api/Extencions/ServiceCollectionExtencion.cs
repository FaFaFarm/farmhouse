﻿using System;
using System.Threading.Tasks;
using FarmHouse.Api.Authentication;
using FarmHouse.Models.Authentication;
using FarmHouse.Models.ViewModels.Users;
using FarmHouse.Models.ViewModels.Users.Farmer;
using FluentValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FarmHouse.Api.Extencions
{
    public static partial class ServiceCollectionExtencion
    {
        public static IServiceCollection AddApiJwtAuthentication(this IServiceCollection services, JwtOptions tokenOptions, IConfiguration appConfiguration)
        {
            if (tokenOptions == null)
            {
                throw new ArgumentNullException($"{nameof(tokenOptions)} is a required parameter. " +
                    "Please make sure you've provided a valid instance with the appropriate values configured.");
            }

            services.AddScoped<IJwtTokenGenerator, JwtTokenGenerator>(serviceProvider => new JwtTokenGenerator(tokenOptions));

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
            {
                options.RequireHttpsMetadata = true;
                options.SaveToken = true;
                options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateAudience = false,
                    ValidAudience = tokenOptions.Audience,
                    ValidateIssuer = false,
                    ValidIssuer = tokenOptions.Issuer,
                    IssuerSigningKey = tokenOptions.SecurityKey,
                    ValidateIssuerSigningKey = true,
                    RequireExpirationTime = true,
                    ValidateLifetime = true
                };

                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        var accessToken = context.Request.Cookies[appConfiguration["Coockies:AuthenticationToken"]];

                        var path = context.HttpContext.Request.Path;

                        if (!string.IsNullOrEmpty(accessToken) && (path.StartsWithSegments("/chat")))
                        {
                            context.Token = accessToken;
                        }

                        return Task.CompletedTask;
                    }
                };
            });

            services.AddCors();

            return services;
        }

        public static IServiceCollection ConfigureFluentValidator(this IServiceCollection services)
        {
            services.AddTransient<IValidator<UserRegisterModel>, UserRegisterModelValidator>();
            services.AddTransient<IValidator<UserAuthenticationModel>, UserAuthenticationModelValidator>();
            services.AddTransient<IValidator<FarmerAdditionalInformation>, FarmerAdditionalInformationValidator>();
            services.AddTransient<IValidator<UserAdditionalInformation>, UserAdditionalInformationValidator>();

            return services;
        }
    }
}
