using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using FarmHouse.DataLayer.DbMigrator;
using Microsoft.AspNetCore.Builder;

namespace FarmHouse.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DbMigrator.Migrate();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
