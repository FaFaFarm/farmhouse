﻿namespace FarmHouse.Models.DBModels.StoreModels.Categories
{
    public sealed class ProductCategory
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public byte[] CategoryImage { get; set; }

        public int GeneralCategoryId { get; set; }

        public GeneralCategory GeneralCategory { get; set; }
    }
}
