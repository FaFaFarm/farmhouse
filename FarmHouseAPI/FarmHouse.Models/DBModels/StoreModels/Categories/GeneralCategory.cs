﻿using System.Collections.Generic;

namespace FarmHouse.Models.DBModels.StoreModels.Categories
{
    public sealed class GeneralCategory
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<ProductCategory> ProductCategories { get; set; }
    }
}
