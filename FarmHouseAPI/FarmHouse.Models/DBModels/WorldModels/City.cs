﻿namespace FarmHouse.Models.DBModels.WorldModels
{
    public class City
    {
        public int Id { get; set; }

        public string CityName { get; set; }

        public int CountryId { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}
