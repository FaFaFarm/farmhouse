﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmHouse.Models.DBModels.UsersModels
{
    public partial class UserCredentials
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string SecondName { get; set; }

        public string Email { get; set; }

        public string HashPassword { get; set; }

        public int CityId { get; set; }

        public int CountryId { get; set; }

        public DateTime RegistrationDate { get; set; }

        public int AccountTypeId { get; set; }

        [ForeignKey("AccountTypeId")]
        public AccountType AccountType { get; set; }

        public AdditionalUserInfo AdditionalUserInfo { get; set; }
    }
}
