﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FarmHouse.Enums;

namespace FarmHouse.Models.DBModels.UsersModels
{
    public sealed class AdditionalUserInfo
    {   
        public int Id { get; set; }

        public string AvatarImage { get; set; }
    
        public int OccupationId { get; set; }

        [ForeignKey("OccupationId")]
        public Occupation Occupation { get; set; }

        public DateTime? BirthdayDate { get; set; }

        public int? LanguageId { get; set; }

        public string MobilePhoneNumber { get; set; }

        public string About { get; set; }

        public int UserCredentialsId { get; set; }

        public UserCredentials UserCredentials { get; set; }
    }
}
