﻿namespace FarmHouse.Models.DBModels.UsersModels
{
    public sealed class Occupation
    {
        public int Id { get; set; }

        public string OccupationName { get; set; }
    }
}
