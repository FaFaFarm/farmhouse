﻿namespace FarmHouse.Models.DBModels.UsersModels
{
    public sealed class AccountType
    {
        public int Id { get; set; }

        public string AccountTypeName { get; set; }
    }
}
