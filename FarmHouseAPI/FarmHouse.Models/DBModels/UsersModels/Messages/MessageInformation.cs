﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FarmHouse.Models.DBModels.UsersModels.Messages
{
    public sealed class MessageInformation
    {
        public int Id { get; set; }

        public int FromUserId { get; set; }

        public int ToUserId { get; set; }

        public string Message { get; set; }

        public bool WithAttachment { get; set; }

        public DateTime SendingDate { get; set; }

        public int? AttachmentTypeId { get; set; }

        [ForeignKey("AttachementsTypeId")]
        public AttachementsTypes AttachementsType { get; set; }
    }
}
