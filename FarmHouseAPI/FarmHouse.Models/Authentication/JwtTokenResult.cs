﻿using System;

namespace FarmHouse.Models.Authentication
{
    public sealed class JwtTokenResult
    {
        public string AccessToken { get; set; }

        public TimeSpan Expires { get; set; }
    }
}
