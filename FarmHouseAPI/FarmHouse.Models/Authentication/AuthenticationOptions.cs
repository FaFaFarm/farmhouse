﻿namespace FarmHouse.Models.Authentication
{
    public sealed class AuthenticationOptions
    {
        public string Issuer { get; set; }

        public string Audience { get; set; }

        public int Lifetime { get; set; }

        public string SecretKey { get; set; }
    }
}
