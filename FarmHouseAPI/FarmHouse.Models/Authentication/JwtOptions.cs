﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace FarmHouse.Models.Authentication
{
    public sealed class JwtOptions
    {
        public JwtOptions(string issuer, string audience, string rawSecurityKey, int tokenExpiryInMinutes = 5)
        {
            Issuer = issuer;
            Audience = audience;
            TokenExpiryInMinutes = tokenExpiryInMinutes;

            SecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(rawSecurityKey));
        }

        public SecurityKey SecurityKey { get; }

        public string Issuer { get; }

        public string Audience { get; }

        public int TokenExpiryInMinutes { get; }
    }
}
