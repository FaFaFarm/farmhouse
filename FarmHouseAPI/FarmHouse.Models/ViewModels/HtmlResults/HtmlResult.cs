﻿namespace FarmHouse.Models.ViewModels.HtmlResults
{
    public class HtmlResult
    {
        public HtmlResult (string html, object metadata)
        {
            Html = html;
            Metadata = metadata;
        }

        public HtmlResult (string html)
        {
            Html = html;
        }

        public string Html { get; set; }

        public object Metadata { get; set; }
    }
}
