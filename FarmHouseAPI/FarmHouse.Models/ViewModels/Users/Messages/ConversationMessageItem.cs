﻿using System;

namespace FarmHouse.Models.ViewModels.Users.Messages
{
    public sealed class ConversationMessageItem
    {
        public string Message { get; set; }

        public bool IsReceivedByCurrentUser { get; set; }  

        public bool IsRead { get; set; }

        public byte[] Attachment { get; set; }

        public bool WithAttachment { get; set; }

        public int InterlocutorUserId { get; set; }

        public DateTime ReceivedDate { get; set; }
    }
}
