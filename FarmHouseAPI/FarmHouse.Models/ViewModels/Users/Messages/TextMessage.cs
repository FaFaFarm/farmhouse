﻿namespace FarmHouse.Models.ViewModels.Users.Messages
{
    public class TextMessage
    {
        public int ToUserId { get; set; }

        public string Message { get; set; }
    }
}
