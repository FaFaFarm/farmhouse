﻿namespace FarmHouse.Models.ViewModels.Users.Messages
{
    public class TextMessageWithAttachment : TextMessage
    {
        public byte[] Attachment { get; set; }
    }
}
