﻿namespace FarmHouse.Models.ViewModels.Users
{
    public sealed class UserSearchModel
    {
        public int UserId { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string AvatarImage { get; set; }
    }
}
