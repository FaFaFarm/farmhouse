﻿using FarmHouse.Enums;
using FarmHouse.Library.Extencions;
using FluentValidation;

namespace FarmHouse.Models.ViewModels.Users
{
    public sealed class UserRegisterModel
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public int CityId { get; set; }

        public int CountryId { get; set; }

        public string AccountType { get; set; }
    }

    public sealed class UserRegisterModelValidator : AbstractValidator<UserRegisterModel>
    {
        public UserRegisterModelValidator()
        {
            RuleFor(x => x.Password).NotNull().NotEmpty().MinimumLength(8).MaximumLength(16);
            RuleFor(x => x.Name).NotNull().NotEmpty();
            RuleFor(x => x.Surname).NotNull().NotEmpty();
            RuleFor(x => x.Email).NotNull().NotEmpty();
            RuleFor(x => x.CityId).NotNull().NotEqual(0);
            RuleFor(x => x.CountryId).NotNull().NotEqual(0);
            RuleFor(x => x.AccountType).Must(z => AccountTypes.Customer.EnumToString().ToLower() == z.ToLower() || AccountTypes.Farmer.EnumToString().ToLower() == z.ToLower());
        }
    }
}

