﻿using FluentValidation;

namespace FarmHouse.Models.ViewModels.Users.Farmer
{
    public sealed class FarmerAdditionalInformation : UserAdditionalInformation
    {
        public string Occupation { get; set; }
    }

    public sealed class FarmerAdditionalInformationValidator : AbstractValidator<FarmerAdditionalInformation>
    {
        public FarmerAdditionalInformationValidator()
        {
            RuleFor(x => x.Occupation).NotNull().NotEmpty();
        }
    }
}
