﻿using FarmHouse.Enums;

namespace FarmHouse.Models.ViewModels.Users
{
    public sealed class UserAuthInformation
    {
        public int Id { get; set; }

        public AccountTypes AccountType { get; set; }
    }
}
