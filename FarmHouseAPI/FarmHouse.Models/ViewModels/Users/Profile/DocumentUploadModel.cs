﻿namespace FarmHouse.Models.ViewModels.Users.Profile
{
    public sealed class DocumentUploadModel
    {
        public string Document { get; set; }

        public int UserId { get; set; }

        public string Description { get; set; }
    }
}
