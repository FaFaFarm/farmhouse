﻿using FluentValidation;

namespace FarmHouse.Models.ViewModels.Users
{
    public sealed class UserAuthenticationModel
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }

    public sealed class UserAuthenticationModelValidator : AbstractValidator<UserAuthenticationModel>
    {
        public UserAuthenticationModelValidator()
        {
            RuleFor(x => x.Password).NotNull().NotEmpty().MinimumLength(8).MaximumLength(16);         
            RuleFor(x => x.Email).NotNull().NotEmpty();
        }
    }
}
