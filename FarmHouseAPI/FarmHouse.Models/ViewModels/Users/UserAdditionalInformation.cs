﻿using System;
using FluentValidation;

namespace FarmHouse.Models.ViewModels.Users
{
    public class UserAdditionalInformation
    {
        public string MobilePhone { get; set; }

        public string About { get; set; }

        public DateTime Birthday { get; set; }

        public byte[] Image { get; set; }
    }

    public sealed class UserAdditionalInformationValidator : AbstractValidator<UserAdditionalInformation>
    {
        public UserAdditionalInformationValidator()
        {
            RuleFor(x => x.About).NotNull().NotEmpty();
            RuleFor(x => x.Birthday).NotNull().NotEmpty().Must(z => z.Year > DateTime.Now.AddYears(-100).Year)
                .Must(z => z.Year < DateTime.Now.AddYears(-5).Year);
            RuleFor(x => x.MobilePhone).NotEmpty().NotNull().Must(z => z.StartsWith('+'));
        }
    }
}
