﻿using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace FarmHouse.Models.ViewModels.HttpResults
{
    public class HttpResult : IHttpResult
    {
        public HttpResult(string error, int statusCode)
        { 
            Errors = new List<string>();
            Errors.Add(error);
            StatusCode = statusCode;
        }

        public HttpResult(List<string> errors, int statusCode)
        {
            Errors = new List<string>(errors);
            StatusCode = statusCode;
        }

        public HttpResult()
        {
            StatusCode = (int)HttpStatusCode.OK;
        }

        public int StatusCode { get; private set; }

        public List<string> Errors { get; private set; }

        public bool IsSuccessful => Errors == null || !Errors.Any() || StatusCode == (int)HttpStatusCode.OK;

        public void AddError(string error)
        {
            if (Errors == null)
            {
                Errors = new List<string>();
            }

            Errors.Add(error);
        }
    }
}
