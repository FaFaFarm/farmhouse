﻿using System.Collections.Generic;

namespace FarmHouse.Models.ViewModels.HttpResults
{
    public interface IHttpResult
    {
        public int StatusCode { get; }

        public List<string> Errors { get; }

        public bool IsSuccessful { get; }

        public void AddError(string error);
    }
}
