﻿namespace FarmHouse.Models.ViewModels.CountryCity
{
    public class CityViewModel
    {
        public int Id { get; set; }

        public int CountryId { get; set; }

        public string Name { get; set; }
    }
}
