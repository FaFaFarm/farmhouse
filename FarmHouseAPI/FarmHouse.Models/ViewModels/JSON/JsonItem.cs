﻿namespace FarmHouse.Models.ViewModels.JSON
{
    public struct JsonItem
    {
        public JsonItem(object json)
        {
            Json = json;
        }

        public object Json { get; set; }
    }
}
