﻿using Moq;
using NUnit.Framework;
using FarmHouse.Core.Logger;

namespace FarmHouse.Core.Test.Logger
{
    [TestFixture]
    internal sealed class FileLoggerTests
    {
        private Mock<FileLogger> _fileLoggerMock;

        private LogProvider _logProvider;

        [SetUp]
        public void SetUp()
        {
            _fileLoggerMock = new Mock<FileLogger>("D://log.txt");

            _logProvider = new LogProvider();
            _logProvider.Add(_fileLoggerMock.Object);
        }
    }
}
