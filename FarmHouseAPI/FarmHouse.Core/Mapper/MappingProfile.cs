﻿using AutoMapper;
using FarmHouse.Models.DBModels.WorldModels;
using FarmHouse.Models.ViewModels.CountryCity;

namespace FarmHouse.Core.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<City, CityViewModel>()
                .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.CountryId))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.CityName));

            CreateMap<Country, CountryViewModel>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.CountryName))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.CountryId));
        }        
    }    
}
