﻿using System;

namespace FarmHouse.Core.Logger
{
    public class ConsoleLogger : ILog
    {
        public void Log(string message)
        {
            Console.WriteLine(GetResultMessage(message));
        }

        private string GetResultMessage(string message)
        {
            return DateTime.Now + " : " + message;
        }
    }
}
