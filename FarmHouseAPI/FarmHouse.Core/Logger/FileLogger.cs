﻿using System;
using System.IO;

namespace FarmHouse.Core.Logger
{
    public class FileLogger : ILog
    {
        private string _filePath;

        public FileLogger(string filePath)
        {
            _filePath = filePath;
        }

        public void Log(string message)
        {
            if (!File.Exists(_filePath))
            {
                File.CreateText(_filePath).Close();
            }

            using (StreamWriter writer = File.AppendText(_filePath))
            {
                writer.WriteLine(GetResultMessage(message));
            }
        }

        private string GetResultMessage(string message)
        {
            return DateTime.Now + " : " + message;
        }
    }
}
