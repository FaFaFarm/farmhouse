﻿using System;
using FarmHouse.DataLayer.Users.Context;
using Microsoft.EntityFrameworkCore;

namespace FarmHouse.DataLayer.DbMigrator
{
    public static class DbMigrator
    {
        public static void Migrate()
        {            
            var usersContext = new UsersContext();

            try
            {
                usersContext.Database.Migrate();
            }           
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
