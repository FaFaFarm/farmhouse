﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FarmHouse.DataLayer
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();

        IEnumerable<T> GetAll(params Expression<Func<T, object>>[] includes);

        IEnumerable<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);

        IEnumerable<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] includes);

        IEnumerable<T> GetPartial(int maxCount, int partitionNumber);

        Task<IEnumerable<T>> GetPartialAsync(int maxCount, int partitionNumber);

        IEnumerable<T> GetPartial(int maxCount, int partitionNumber, params Expression<Func<T, object>>[] includes);

        Task<IEnumerable<T>> GetPartialAsync(int maxCount, int partitionNumber, params Expression<Func<T, object>>[] includes);

        T GetById(int id);

        T GetById(int id, params Expression<Func<T, object>>[] includes);

        Task<T> GetByIdAsync(int id);

        Task<T> GetByIdAsync(int id, params Expression<Func<T, object>>[] includes);

        void Add(T record);

        Task AddAsync(T record);

        void Remove(T record);

        void Remove(int id);

        void Edit(T record);
    }
}
