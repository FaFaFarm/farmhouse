﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FarmHouse.Models.DBModels.WorldModels;

namespace FarmHouse.DataLayer.World.Repositories
{
    public interface ICityRepository : IRepository<City>
    {
        Task<List<City>> GetCitiesBySubStringAsync(string searchString);

        Task<List<City>> GetCitiesByCountryAndSubStringAsync(string searchString, int countryId);

        Task<List<City>> GetCitiesByCountyAsync(int countryId);
    }
}
