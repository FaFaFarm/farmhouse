﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FarmHouse.Models.DBModels.WorldModels;

namespace FarmHouse.DataLayer.World.Repositories
{
    public interface ICountryRepository : IRepository<Country>
    {
        Task<List<Country>> GetCountriesBySubStringAsync(string searchString);
    }
}
