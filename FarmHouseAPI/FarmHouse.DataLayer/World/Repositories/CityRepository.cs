﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmHouse.Models.DBModels.WorldModels;
using FarmHouse.DataLayer.World.Contexts;
using Microsoft.EntityFrameworkCore;

namespace FarmHouse.DataLayer.World.Repositories
{
    public sealed class CityRepository : BaseWorldRepository<City>, ICityRepository
    {
        private readonly WorldContext _worldContext;

        public CityRepository(WorldContext worldContext) : base(worldContext)
        {
            _worldContext = worldContext ?? throw new NullReferenceException(nameof(worldContext));
        }

        public async Task<List<City>> GetCitiesBySubStringAsync(string searchString)
        {
            return await _worldContext.Cities.Where(x => x.CityName.StartsWith(searchString)).AsQueryable().ToListAsync();
        }

        public async Task<List<City>> GetCitiesByCountryAndSubStringAsync(string searchString, int countryId)
        {
            return await _worldContext.Cities.Where(z => z.CountryId == countryId && z.CityName.StartsWith(searchString)).AsQueryable().ToListAsync();
        }

        public async Task<List<City>> GetCitiesByCountyAsync(int countryId)
        {
            return await _worldContext.Cities.Where(x => x.CountryId == countryId).AsQueryable().ToListAsync();
        }
    }
}
