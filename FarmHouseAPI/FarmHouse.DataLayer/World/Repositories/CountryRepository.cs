﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmHouse.Models.DBModels.WorldModels;
using FarmHouse.DataLayer.World.Contexts;
using Microsoft.EntityFrameworkCore;

namespace FarmHouse.DataLayer.World.Repositories
{
    public sealed class CountryRepository : BaseWorldRepository<Country>, ICountryRepository
    {
        private readonly WorldContext _worldContext;

        public CountryRepository(WorldContext worldContext) : base(worldContext)
        {
            _worldContext = worldContext ?? throw new NullReferenceException(nameof(worldContext));
        }

        public async Task<List<Country>> GetCountriesBySubStringAsync(string searchString)
        {
            return await _worldContext.Countries.Where(x => x.CountryName.StartsWith(searchString)).AsQueryable().ToListAsync();
        }
    }
}
