﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using FarmHouse.DataLayer.World.Contexts;
using Microsoft.EntityFrameworkCore;

namespace FarmHouse.DataLayer
{
    public abstract class BaseWorldRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly WorldContext _worldContext;

        private readonly DbSet<TEntity> _dbSet;

        public BaseWorldRepository(WorldContext worldContext)
        {
            _worldContext = worldContext ?? throw new NullReferenceException(nameof(worldContext));

            _dbSet = worldContext.Set<TEntity>();
        }

        public virtual void Add(TEntity record)
        {
            if (record == null)
            {
                throw new ArgumentNullException(nameof(record));
            }

            _dbSet.Add(record);
            _worldContext.SaveChanges();
        }

        public virtual async Task AddAsync(TEntity record)
        {
            if (record == null)
            {
                throw new ArgumentNullException(nameof(record));
            }

            await _dbSet.AddAsync(record);
            await _worldContext.SaveChangesAsync();
        }

        public virtual void Edit(TEntity record)
        {
            if (record == null)
            {
                throw new ArgumentNullException(nameof(record));
            }

            _dbSet.Update(record);
            _worldContext.SaveChanges();
        }

        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
           
            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = _dbSet;

            foreach (var include in includes)
            {
                query.Include(include);
            }

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _dbSet;
        }

        public virtual IEnumerable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = _dbSet;

            foreach (var include in includes)
            {
                query.Include(include);
            }

            return query;
        }

        public TEntity GetById(int id)
        {
            return _dbSet.Find(id);
        }

        public virtual TEntity GetById(int id, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = _dbSet;

            foreach (var include in includes)
            {
                query.Include(include);
            }

            return _dbSet.Find(id);
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public async virtual Task<TEntity> GetByIdAsync(int id, params Expression<Func<TEntity, object>>[] includes)
        {
            foreach (var include in includes)
            {
                _dbSet.Include(include);
            }

            return await _dbSet.FindAsync(id);
        }

        public IEnumerable<TEntity> GetPartial(int maxCount, int partitionNumber)
        {
            IQueryable<TEntity> query = _dbSet;

            return query.Skip(partitionNumber).Take(maxCount);
        }

        public IEnumerable<TEntity> GetPartial(int maxCount, int partitionNumber, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = _dbSet;

            foreach (var include in includes)
            {
                query.Include(include);
            }    

            return query.Skip(partitionNumber).Take(maxCount);
        }

        public async Task<IEnumerable<TEntity>> GetPartialAsync(int maxCount, int partitionNumber)
        {
            IQueryable<TEntity> query = _dbSet;

            return await query.Skip(partitionNumber).Take(maxCount).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetPartialAsync(int maxCount, int partitionNumber, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = _dbSet;

            foreach (var include in includes)
            {
                query.Include(include);
            }

            return await query.Skip(partitionNumber).Take(maxCount).ToListAsync();
        }

        public virtual void Remove(TEntity record)
        {
            if (record == null)
            {
                throw new ArgumentNullException(nameof(record));
            }

            if (_worldContext.Entry(record).State == EntityState.Detached)
            {
                _dbSet.Attach(record);
            }

            _dbSet.Remove(record);
        }

        public virtual void Remove(int id)
        {
            TEntity entityToDelete = _dbSet.Find(id);
            Remove(entityToDelete);
        }
    }
}
