﻿using System;
using FarmHouse.DataLayer.Users.Repositories;
using FarmHouse.DataLayer.Users.Repositories.Messages;
using FarmHouse.DataLayer.Users.Repositories.Store;

namespace FarmHouse.DataLayer.Users.UnitOfWork
{
    public interface IUnitOFWork : IDisposable
    {
        IUserCredentialsRepository UserCredentialsRepository { get; }

        IUserInfoRepository UserInfoRepository { get; }

        IUserOccupationRepository UserOccupationRepository { get; }

        IGeneralCategoriesRepository GeneralCategoriesRepository { get; }

        ISubCategoriesRepository SubCategoriesRepository { get; }

        IMessageRepository MessageRepository { get; }

        void Save();
    }
}
