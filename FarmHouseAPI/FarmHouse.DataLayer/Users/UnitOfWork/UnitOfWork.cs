﻿using System;
using FarmHouse.DataLayer.Users.Context;
using FarmHouse.DataLayer.Users.Repositories;
using FarmHouse.DataLayer.Users.Repositories.Messages;
using FarmHouse.DataLayer.Users.Repositories.Store;

namespace FarmHouse.DataLayer.Users.UnitOfWork
{
    public sealed class UnitOfWork : IUnitOFWork
    {
        private IUserCredentialsRepository _userCredentialsRepository;
        private IUserInfoRepository _userInfoRepository;
        private IUserOccupationRepository _userOccupationRepository;
        private IGeneralCategoriesRepository _generalCategoriesRepository;
        private ISubCategoriesRepository _subCategoriesRepository;
        private IMessageRepository _messageRepository;

        private bool disposed;

        private readonly UsersContext _usersContext;

        public UnitOfWork(UsersContext usersContext)
        {
            _usersContext = usersContext ?? throw new NullReferenceException(nameof(usersContext));
        }

        public IUserCredentialsRepository UserCredentialsRepository
        {
            get
            {
                if (_userCredentialsRepository == null)
                {
                    _userCredentialsRepository = new UserCredentialsRepository(_usersContext);
                }

                return _userCredentialsRepository;
            }
        }

        public IUserInfoRepository UserInfoRepository
        {
            get
            {
                if (_userInfoRepository == null)
                {
                    _userInfoRepository = new UserInfoRepository(_usersContext);
                }

                return _userInfoRepository;
            }
        }

        public IUserOccupationRepository UserOccupationRepository
        {
            get
            {
                if (_userOccupationRepository == null)
                {
                    _userOccupationRepository = new UserOccupationRepository(_usersContext);
                }

                return _userOccupationRepository;
            }
        }

        public IGeneralCategoriesRepository GeneralCategoriesRepository
        {
            get
            {
                if (_generalCategoriesRepository == null)
                {
                    _generalCategoriesRepository = new GeneralCategoriesRepository(_usersContext);
                }

                return _generalCategoriesRepository;
            }
        }

        public ISubCategoriesRepository SubCategoriesRepository
        {
            get
            {
                if (_subCategoriesRepository == null)
                {
                    _subCategoriesRepository = new SubCategoriesRepository(_usersContext);
                }

                return _subCategoriesRepository;
            }
        }

        public IMessageRepository MessageRepository
        {
            get
            {
                if (_messageRepository == null)
                {
                    _messageRepository = new MessageRepository(_usersContext);
                }

                return _messageRepository;
            }
        }

        public void Save()
        {
            _usersContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _usersContext.Dispose();
                }

                disposed = true;
            }
        }
    }
}
