﻿using System;
using Microsoft.EntityFrameworkCore;
using FarmHouse.Models.DBModels.UsersModels;
using FarmHouse.Models.DBModels.UsersModels.Messages;
using FarmHouse.Models.DBModels.StoreModels.Categories;
using Microsoft.AspNetCore.Hosting;

namespace FarmHouse.DataLayer.Users.Context
{
    public partial class UsersContext : DbContext
    {
        private readonly IHostingEnvironment _environment;

        public UsersContext()
        {
        }

        public UsersContext(DbContextOptions<UsersContext> options, IHostingEnvironment environment)
            : base(options)
        {
            _environment = environment ?? throw new NullReferenceException();
        }

        public virtual DbSet<AdditionalUserInfo> AdditionalUserInfo { get; set; }

        public virtual DbSet<UserCredentials> UserCredentials { get; set; }

        public virtual DbSet<Occupation> Occupations { get; set; }

        public virtual DbSet<AccountType> AccountTypes { get; set; }

        public virtual DbSet<AttachementsTypes> AttachementsTypes { get; set; }

        public virtual DbSet<MessageInformation> MessageInfo { get; set; }

        public virtual DbSet<GeneralCategory> GeneralCategory { get; set; }

        public virtual DbSet<ProductCategory> ProductCategory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                if (_environment.IsDevelopment())
                {
                    optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=Users;Username=postgres;Password=09042000");
                }
                else
                {
                    optionsBuilder.UseNpgsql("postgres://hqtekymhgdpeux:fca1cb9397fd76e7ea4c4c1213799a9e1f360265431163fa1d47a5299743f676@ec2-54-195-247-108.eu-west-1.compute.amazonaws.com:5432/dauvrcpmh4235r");
                }
            }
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
