﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmHouse.DataLayer.Users.Context;
using FarmHouse.Models.DBModels.UsersModels;
using FarmHouse.Models.ViewModels.Users;
using Microsoft.EntityFrameworkCore;

namespace FarmHouse.DataLayer.Users.Repositories
{
    public sealed class UserInfoRepository : BaseUsersRepository<AdditionalUserInfo>, IUserInfoRepository
    {
        private readonly UsersContext _usersContext;

        public UserInfoRepository(UsersContext usersContext) : base(usersContext)
        {
            _usersContext = usersContext ?? throw new NullReferenceException(nameof(usersContext));
        }

        public async Task<List<UserSearchModel>> GetUsersByFullNameSubStringAsync(string fullNameSubString)
        {
            IQueryable<UserSearchModel> query = from p in _usersContext.AdditionalUserInfo
                                                join c in _usersContext.UserCredentials on p.UserCredentialsId equals c.Id
                                                select new UserSearchModel { Name = c.Name, Surname = c.SecondName, UserId = c.Id, AvatarImage = p.AvatarImage };

            return await query.Where(p => (p.Name + " " + p.Surname).StartsWith(fullNameSubString)).ToListAsync();
        }
    }
}
