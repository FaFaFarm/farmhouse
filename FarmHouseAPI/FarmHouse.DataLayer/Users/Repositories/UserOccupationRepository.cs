﻿using System;
using FarmHouse.DataLayer.Users.Context;
using FarmHouse.Models.DBModels.UsersModels;

namespace FarmHouse.DataLayer.Users.Repositories
{
    public sealed class UserOccupationRepository : BaseUsersRepository<Occupation>, IUserOccupationRepository
    {
        private readonly UsersContext _usersContext;

        public UserOccupationRepository(UsersContext usersContext) : base(usersContext)
        {
            _usersContext = usersContext ?? throw new NullReferenceException(nameof(usersContext));
        }        
    }
}
