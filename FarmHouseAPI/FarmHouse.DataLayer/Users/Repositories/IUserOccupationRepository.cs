﻿using FarmHouse.Models.DBModels.UsersModels;

namespace FarmHouse.DataLayer.Users.Repositories
{
    public interface IUserOccupationRepository : IRepository<Occupation>
    {

    }
}
