﻿using System;
using FarmHouse.DataLayer.Users.Context;
using FarmHouse.Models.DBModels.StoreModels.Categories;

namespace FarmHouse.DataLayer.Users.Repositories.Store
{
    public sealed class SubCategoriesRepository : BaseUsersRepository<ProductCategory>, ISubCategoriesRepository
    {
        private readonly UsersContext _usersContext;

        public SubCategoriesRepository(UsersContext usersContext) : base(usersContext)
        {
            _usersContext = usersContext ?? throw new NullReferenceException(nameof(usersContext));
        }
    }
}
