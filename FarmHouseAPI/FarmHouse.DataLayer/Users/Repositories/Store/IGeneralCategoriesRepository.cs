﻿using FarmHouse.Models.DBModels.StoreModels.Categories;

namespace FarmHouse.DataLayer.Users.Repositories.Store
{
    public interface IGeneralCategoriesRepository : IRepository<GeneralCategory>
    {
    }
}
