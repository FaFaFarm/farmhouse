﻿using FarmHouse.Models.DBModels.StoreModels.Categories;

namespace FarmHouse.DataLayer.Users.Repositories.Store
{
    public interface ISubCategoriesRepository : IRepository<ProductCategory>
    {
    }
}
