﻿using System;
using System.Collections.Generic;
using System.Linq;
using FarmHouse.DataLayer.Users.Context;
using FarmHouse.Models.DBModels.StoreModels.Categories;

namespace FarmHouse.DataLayer.Users.Repositories.Store
{
    public sealed class GeneralCategoriesRepository : BaseUsersRepository<GeneralCategory>, IGeneralCategoriesRepository
    {
        private readonly UsersContext _usersContext;

        public GeneralCategoriesRepository(UsersContext usersContext) : base(usersContext)
        {
            _usersContext = usersContext ?? throw new NullReferenceException(nameof(usersContext));
        }       
    }
}
