﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmHouse.DataLayer.Users.Context;
using FarmHouse.Models.DBModels.UsersModels;
using Microsoft.EntityFrameworkCore;

namespace FarmHouse.DataLayer.Users.Repositories
{
    public sealed class UserCredentialsRepository : BaseUsersRepository<UserCredentials>, IUserCredentialsRepository
    {
        private readonly UsersContext _usersContext;

        public UserCredentialsRepository(UsersContext usersContext) : base(usersContext)
        {
            _usersContext = usersContext ?? throw new NullReferenceException(nameof(usersContext));
        }

        public async Task<List<UserCredentials>> GetUsersByEmailAsync(string email)
        {
            return await _usersContext.UserCredentials.Where(x => x.Email == email).AsQueryable().ToListAsync();
        }

        public override UserCredentials GetById(int id)
        {
            return _usersContext.UserCredentials.Include(x => x.AdditionalUserInfo).Where(x => x.Id == id).FirstOrDefault();
        }

        public override async Task<UserCredentials> GetByIdAsync(int id)
        {
            return await _usersContext.UserCredentials.Include(x => x.AdditionalUserInfo).Where(x => x.Id == id).AsQueryable().FirstOrDefaultAsync();
        }

        public override async Task AddAsync(UserCredentials record)
        {
            if (record == null)
            {
                throw new ArgumentNullException(nameof(record));
            }

            await _usersContext.UserCredentials.AddAsync(record);
            await _usersContext.SaveChangesAsync();

            await _usersContext.AdditionalUserInfo.AddAsync(new AdditionalUserInfo { UserCredentialsId = record.Id, OccupationId = 1 });
            await _usersContext.SaveChangesAsync();
        }
    }
}
