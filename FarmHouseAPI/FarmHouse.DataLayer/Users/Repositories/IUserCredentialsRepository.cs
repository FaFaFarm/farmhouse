﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FarmHouse.Models.DBModels.UsersModels;

namespace FarmHouse.DataLayer.Users.Repositories
{
    public interface IUserCredentialsRepository : IRepository<UserCredentials>
    {
        Task<List<UserCredentials>> GetUsersByEmailAsync(string email);
    }
}
