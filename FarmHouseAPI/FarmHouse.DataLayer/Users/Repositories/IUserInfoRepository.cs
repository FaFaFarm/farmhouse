﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FarmHouse.Models.DBModels.UsersModels;
using FarmHouse.Models.ViewModels.Users;

namespace FarmHouse.DataLayer.Users.Repositories
{
    public interface IUserInfoRepository : IRepository<AdditionalUserInfo>
    {
        Task<List<UserSearchModel>> GetUsersByFullNameSubStringAsync(string fullNameSubString);
    }
}
