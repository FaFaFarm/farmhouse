﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FarmHouse.DataLayer.Users.Context;
using FarmHouse.Models.DBModels.UsersModels.Messages;
using Microsoft.EntityFrameworkCore;

namespace FarmHouse.DataLayer.Users.Repositories.Messages
{
    public sealed class MessageRepository : BaseUsersRepository<MessageInformation>, IMessageRepository
    {
        private readonly UsersContext _usersContext;

        public MessageRepository(UsersContext usersContext) : base(usersContext)
        {
            _usersContext = usersContext ?? throw new NullReferenceException(nameof(usersContext));
        }

        public async Task<List<MessageInformation>> GetMessagesForCurrentUser(int currentUserId)
        {
            return null;//  return await _usersContext.MessageInfo.Where(p => p.FromUserId == currentUserId || p.ToUserId == currentUserId).AsQueryable().ToListAsync();
        }
    }
}
