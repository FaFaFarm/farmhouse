﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FarmHouse.Models.DBModels.UsersModels.Messages;

namespace FarmHouse.DataLayer.Users.Repositories.Messages
{
    public interface IMessageRepository : IRepository<MessageInformation>
    {
        Task<List<MessageInformation>> GetMessagesForCurrentUser(int currentUserId);
    }
}
