﻿namespace FarmHouse.Enums
{
    public enum AccountTypes
    {
        Customer = 1,
        Farmer = 2
    }
}
