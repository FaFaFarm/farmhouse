﻿namespace FarmHouse.Enums
{
    public enum Roles
    {
        CoreAdmin = 1,
        Farmer = 2,
        Customer = 3,
        Authorized = 4,
        UnAuthorized = 5
    }
}
