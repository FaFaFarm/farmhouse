﻿using System.Collections.Generic;

namespace CountryTownFiller.Models.GeoDB.ResponseModels
{
    public class BaseResponse
    {
        public List<CountryCityModel> Data { get; set; }

        public List<LinkModel> Links { get; set; }
    }
}
