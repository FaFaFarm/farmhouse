﻿namespace CountryTownFiller.Models.GeoDB.ResponseModels
{
    public class LinkModel
    {
        public string Rel { get; set; }

        public string Href { get; set; }
    }
}
