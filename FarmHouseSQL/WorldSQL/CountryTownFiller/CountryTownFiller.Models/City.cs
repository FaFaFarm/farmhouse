﻿using System.ComponentModel;

namespace CountryTownFiller.Models
{
    public class City
    {
        public string CityName { get; set; }

        public int Id { get; set; }

        public int CountryId { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}
