﻿using System.ComponentModel;

namespace CountryTownFiller.Models
{
    public class Country
    {
        public string CountryName { get; set; }

        public int CountryId { get; set; }        
    }
}
