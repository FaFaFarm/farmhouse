﻿using System.Collections.Generic;
using CountryTownFiller.DL.Contexts;
using CountryTownFiller.Models;
using CountryTownFiller.Models.GeoDB.ResponseModels;
using Microsoft.EntityFrameworkCore;

namespace CountryTownFiller.DL
{
    public interface IDataProvider
    {
        IEnumerable<T> GetAllCounryCities<T>();

        void FillDBByCountries(IEnumerable<Country> countries, DbContextOptions<CountryContext> options);

        void FillDBByCities(IEnumerable<City> cities, DbContextOptions<CityContext> options);
    }
}
