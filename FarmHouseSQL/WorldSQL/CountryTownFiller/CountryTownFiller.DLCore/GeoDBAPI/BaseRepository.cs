﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CountryTownFiller.DL.GeoDBAPI
{
    public abstract class BaseRepository : IDisposable
    {
        public abstract void Dispose();

        public abstract Task<IEnumerable<dynamic>> GetFromApiAsync();
    }
}
