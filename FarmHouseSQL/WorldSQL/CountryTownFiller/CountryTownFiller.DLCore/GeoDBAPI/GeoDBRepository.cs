﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CountryTownFiller.Models.GeoDB.ResponseModels;
using Newtonsoft.Json;

namespace CountryTownFiller.DL.GeoDBAPI
{
    public class GeoDBRepository : BaseRepository
    {
        private HttpClient _httpClient => new HttpClient();
        private bool disposed = false;

        public override async Task<IEnumerable<dynamic>> GetFromApiAsync()
        {
            var responses = new List<BaseResponse>();
            var countryCityModels = new List<CountryCityModel>();
            BaseResponse data = null;
            HttpResponseMessage firstHttpResponse = GetFirstResponce().GetAwaiter().GetResult();
            HttpResponseMessage httpResponse;

            if (firstHttpResponse.IsSuccessStatusCode)
            {
                string firstJson = await firstHttpResponse.Content.ReadAsStringAsync();

                try
                {
                    data = JsonConvert.DeserializeObject<BaseResponse>(firstJson);
                }
                catch
                {
                    throw new Exception();
                }

                responses.Add(data);
            }

            if (data != null && responses.Any())
            {
                while (responses.Last().Links.Any(x => x.Rel == Identifiers.Next))
                {
                    httpResponse = GetResponse(responses.Last().Links.Where(x => x.Rel == Identifiers.Next).FirstOrDefault().Href).GetAwaiter().GetResult();
                    string json = await httpResponse.Content.ReadAsStringAsync();

                    try
                    {
                        data = JsonConvert.DeserializeObject<BaseResponse>(json);
                    }
                    catch
                    {
                        throw new Exception();
                    };

                    responses.Add(data);
                }
            }
            else
            {
                return new List<CountryCityModel>();
            }

            responses.ForEach(x => x.Data.ForEach(y => countryCityModels.Add(y)));

            return countryCityModels;
        }

        private async Task<HttpResponseMessage> GetFirstResponce()
        {
            var response = new HttpResponseMessage();

            for (int ret = 0; ret < 3; ret++)
            {
                try
                {
                    response = await _httpClient.GetAsync(String.Format(Identifiers.BaseApiGetUrl, Identifiers.GetFirstTenCities));
                    Thread.Sleep(500);
                    break;
                }
                catch (Exception)
                {
                    Thread.Sleep(3000);
                }
            }

            return response;
        }

        private async Task<HttpResponseMessage> GetResponse(string nextTenElements)
        {
            var response = new HttpResponseMessage();

            for (int ret = 0; ret < 3; ret++)
            {
                try
                {
                    response = await _httpClient.GetAsync(String.Format(Identifiers.BaseApiGetUrl, nextTenElements));
                    Thread.Sleep(500);
                    break;
                }
                catch (Exception)
                {
                    Thread.Sleep(3000);
                }
            }

            return response;
        }
     
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed) 
            {
                if (disposing)
                {
                    _httpClient.Dispose();
                }

                disposed = true;
            }
        }

        public override void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
