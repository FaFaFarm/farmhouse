﻿using System.Linq;
using System.Collections.Generic;
using CountryTownFiller.DL.Contexts;
using CountryTownFiller.DL.Factory;
using CountryTownFiller.DL.GeoDBAPI;
using CountryTownFiller.Models;
using Microsoft.Extensions.Logging;
using CountryTownFiller.Core.Logs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace CountryTownFiller.DL
{
    public class DataProvider : IDataProvider
    {       
        public IEnumerable<T> GetAllCounryCities<T>()
        {          
            using (BaseRepository repository = RepositoryFactory.GetRepositoryType<T>())
            {
                return repository.GetFromApiAsync().GetAwaiter().GetResult() as IEnumerable<T>;
            }
        }

        public void FillDBByCountries(IEnumerable<Country> countries, DbContextOptions<CountryContext> options)
        {
            using (CountryContext context = new CountryContext(options))
           {
                context.GetService<ILoggerFactory>().AddProvider(new LoggerProvider());
                countries.ToList().ForEach(x => context.Countries.Add(x));
                context.SaveChanges();
            }
        }

        public void FillDBByCities(IEnumerable<City> cities, DbContextOptions<CityContext> options)
        {
            using (CityContext context = new CityContext(options))
            {
                context.GetService<ILoggerFactory>().AddProvider(new LoggerProvider());
                cities.ToList().ForEach(x => context.Cities.Add(x));
                context.SaveChanges();
            }
        }
    }
}
