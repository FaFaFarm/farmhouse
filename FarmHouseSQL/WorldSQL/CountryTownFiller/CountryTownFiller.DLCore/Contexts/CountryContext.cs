﻿using CountryTownFiller.Models;
using Microsoft.EntityFrameworkCore;

namespace CountryTownFiller.DL.Contexts
{
    public class CountryContext : DbContext
    {
        public CountryContext(DbContextOptions<CountryContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        public DbSet<Country> Countries { get; set; }
    }
}
