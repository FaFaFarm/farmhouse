﻿using CountryTownFiller.DL;
using CountryTownFiller.DL.Contexts;
using CountryTownFiller.Models;
using CountryTownFiller.Models.GeoDB.ResponseModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CountryTownFiller.Logic
{
    public class CountryCityDbFillLogic : ICountryCityDbFillLogic
    {
        private readonly IDataProvider _dataProvider;
        
        public CountryCityDbFillLogic(IDataProvider dataProvider)
        {
            if (dataProvider == null)
            {
                throw new NullReferenceException(nameof(dataProvider));
            }

            _dataProvider = dataProvider;
        }

        public void FillDB(IEnumerable<City> cities, IEnumerable<Country> countries, DbContextOptions<CityContext> options)
        {
            _dataProvider.FillDB(cities, countries, options);            
        }

        public IEnumerable<CountryCityModel> GetAllCountryCities()
        {
            return _dataProvider.GetAllCounryCities<CountryCityModel>();
        }            
    }
}
