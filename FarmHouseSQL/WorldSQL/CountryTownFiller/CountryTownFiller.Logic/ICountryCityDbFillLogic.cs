﻿using CountryTownFiller.DL.Contexts;
using CountryTownFiller.Models;
using CountryTownFiller.Models.GeoDB.ResponseModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace CountryTownFiller.Logic
{
    public interface ICountryCityDbFillLogic
    {
        void FillDB(IEnumerable<City> cities, IEnumerable<Country> countries, DbContextOptions<CityContext> options);

        IEnumerable<CountryCityModel> GetAllCountryCities();
    }
}
