﻿using System.Collections.Generic;
using CountryTownFiller.DL.Contexts;
using CountryTownFiller.Models;
using Microsoft.EntityFrameworkCore;

namespace CountryTownFiller.DL
{
    public interface IDataProvider
    {
        IEnumerable<T> GetAllCounryCities<T>();

        void FillDB(IEnumerable<City> cities, IEnumerable<Country> countries, DbContextOptions<CityContext> options);
    }
}
