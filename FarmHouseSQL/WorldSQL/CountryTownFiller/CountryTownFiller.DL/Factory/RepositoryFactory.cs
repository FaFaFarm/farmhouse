﻿using CountryTownFiller.DL.GeoDBAPI;
using CountryTownFiller.Models.GeoDB.ResponseModels;

namespace CountryTownFiller.DL.Factory
{
    public static class RepositoryFactory
    {
        public static BaseRepository GetRepositoryType<T>()
        {
            if (typeof(T) == typeof(CountryCityModel))
            {
                return new GeoDBRepository();
            }

            return null;
        }
    }
}
