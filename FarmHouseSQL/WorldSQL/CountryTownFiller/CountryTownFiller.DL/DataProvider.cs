﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CountryTownFiller.DL.Contexts;
using CountryTownFiller.DL.Factory;
using CountryTownFiller.DL.GeoDBAPI;
using CountryTownFiller.Models;
using Microsoft.EntityFrameworkCore;

namespace CountryTownFiller.DL
{
    public class DataProvider : IDataProvider
    {       
        public IEnumerable<T> GetAllCounryCities<T>()
        {          
            using (BaseRepository repository = RepositoryFactory.GetRepositoryType<T>())
            {
                return repository.GetFromApiAsync().GetAwaiter().GetResult() as IEnumerable<T>;
            }
        }

        public void FillDB(IEnumerable<City> cities, IEnumerable<Country> countries, DbContextOptions<CityContext> options)
        {
            Fill(cities, countries, options).Wait();
        }

        private async Task Fill(IEnumerable<City> cities, IEnumerable<Country> countries, DbContextOptions<CityContext> options)
        {
            using (CityContext context = new CityContext(options))
            {
                context.Cities.AddRange(cities);
                context.Database.OpenConnection();
                try
                {
                    Console.WriteLine("Start saving");
                    //context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT [dbo].[Cities] ON");
                    await context.SaveChangesAsync();
                    //context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Cities OFF");
                }
                finally
                {
                    context.Database.CloseConnection();
                }

                context.Countries.AddRange(countries);
                context.Database.OpenConnection();

                try
                {
                    Console.WriteLine("Start saving");
                    //context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT [dbo].[Countries] ON");
                    await context.SaveChangesAsync();
                    //context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.Countries OFF");
                }
                finally
                {
                    context.Database.CloseConnection();
                }
            }
        }
    }
}
