﻿using CountryTownFiller.Models;
using Microsoft.EntityFrameworkCore;

namespace CountryTownFiller.DL.Contexts
{
    public class CityContext : DbContext
    {
        public CityContext(DbContextOptions<CityContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
    }
}
