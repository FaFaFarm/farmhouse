﻿using System.Configuration;

namespace CountryTownFiller
{
    public static class Identifiers
    {
        public static string BaseApiGetUrl => ConfigurationManager.AppSettings["BaseApiGetUrl"];

        public static string GetFirstTenCities => ConfigurationManager.AppSettings["GetFirstTenCities"];

        public static string Next => ConfigurationManager.AppSettings["Next"];
    }
}
