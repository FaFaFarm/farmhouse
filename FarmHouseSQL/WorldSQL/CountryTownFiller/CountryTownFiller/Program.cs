﻿using CountryTownFiller.DL;
using CountryTownFiller.DL.Contexts;
using CountryTownFiller.Logic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace CountryTownFiller
{
    class Program
    {
        static void Main(string[] args)
        {
            var collection = new ServiceCollection();
            collection.AddSingleton<IDataProvider, DataProvider>();
            collection.AddTransient<ICountryCityDbFillLogic>(x => new CountryCityDbFillLogic(new DataProvider()));
            var serviceProvider = collection.BuildServiceProvider();

            var service = serviceProvider.GetService<ICountryCityDbFillLogic>();

            var filler = new DBFiller(service);

            filler.FillDB();
        }
    }
}
