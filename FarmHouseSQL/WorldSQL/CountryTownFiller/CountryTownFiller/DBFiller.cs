﻿using CountryTownFiller.DL.Contexts;
using CountryTownFiller.Logic;
using CountryTownFiller.Models;
using CountryTownFiller.Models.GeoDB.ResponseModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CountryTownFiller
{
    public class DBFiller
    {
        private readonly ICountryCityDbFillLogic _countryCityDbFillLogic;

        public DBFiller(ICountryCityDbFillLogic countryCityDbFillLogic)
        {
            if (countryCityDbFillLogic == null)
            {
                throw new NullReferenceException(nameof(countryCityDbFillLogic));
            }

            _countryCityDbFillLogic = countryCityDbFillLogic;
        }

        public void FillDB()
        {
            int id = 1;

            IEnumerable<CountryCityModel> countryCityModels = _countryCityDbFillLogic.GetAllCountryCities();
            
            var countries = new List<Country>();

            var builder = new ConfigurationBuilder();

            builder.SetBasePath(Directory.GetCurrentDirectory());

            builder.AddJsonFile("appsettings.json");

            var config = builder.Build();

            string connectionString = config.GetConnectionString("DefaultConnection");           

            var cityOptions = new DbContextOptionsBuilder<CityContext>()
                .UseNpgsql(connectionString)
                .Options;

            foreach (CountryCityModel countryCityModel in countryCityModels)
            {
                if (!countries.Any(x => x.CountryName.Equals(countryCityModel.Country)))
                {
                    countries.Add(new Country() { CountryName = countryCityModel.Country, CountryId = id });
                    id++;
                }
            }

            IEnumerable<City> cities = countryCityModels.Select(
                x => new City() {
                    Id = x.Id,
                    CityName = x.City,
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    CountryId = countries.Where(y => y.CountryName.Equals(x.Country)).FirstOrDefault().CountryId});

            _countryCityDbFillLogic.FillDB(cities, countries, cityOptions);
        }
    }
}
