GO
DROP DATABASE IF EXISTS Users;
GO
CREATE DATABASE Users;
GO
USE Users;
CREATE TABLE [dbo].[UserCredentials](
	[Id] [int] NOT NULL,
	[Name] [nchar](100) NOT NULL,
	[SecondName] [nchar](100) NOT NULL,
	[Email] [nchar](100) NOT NULL,
	[HashPassword] [nchar](100) NOT NULL,
	[CityId] [int] NOT NULL,
	[MobilePhoneNumber] [nchar](14) NULL,
	[CountryId] [int] NOT NULL,
	[LanguageId] [int] NULL,
	[BirthdayDate] [datetime2](7) NULL,
	[Occupation] [nchar](70) NULL,
	[RegistrationDate] [datetime2](7) NOT NULL,
	[AccountTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
GO
